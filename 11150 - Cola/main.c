#include<stdio.h>

int main(){

	int Cocas, loan, enjoy, empty, new, max;

	freopen("input.in", "r", stdin);

	while(scanf("%d", &Cocas) != EOF){
		max = 0;
		for(loan = 2 ; loan >= 0 ; loan--){
			enjoy = Cocas;
			empty = Cocas + loan;
			while(empty > 2){
				new = empty/3;
				enjoy += new;
				empty = new + empty%3;
			}
			if(max < enjoy && empty >= loan){
				max = enjoy;
			}
		}
		printf("%d\n", max);
	}

	return 0;
}
