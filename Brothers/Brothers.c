#include<stdio.h>

int main(){

	int R;
	int C;
	int N;
	int K;
	int x;
	int y;
	int z;
	int Before[102][102];
	int After[102][102];

	while(scanf("%d %d %d %d", &N ,&R, &C, &K)){

		if(N == 0 && R == 0 && N == 0 && K == 0)
			break;

		for(x = 0 ; x < 102 ; x++)
			for(y = 0; y < 102 ; y++){
				Before[x][y] = 200;
				After[x][y] = 200;
			}

		for(x = 1 ; x <= R ; x++)
			for(y = 1 ; y <= C ; y++)
				scanf("%d", &Before[x][y]);

		for(z = 0 ; z < K ; z++){
			for(x = 1 ; x <= R ; x++){
				for(y = 1 ; y <= C ; y++){
					if(After[x][y] == 200)
						After[x][y] = Before[x][y];
					if(Before[x][y] != (N - 1)){
						if(Before[x][y] == (Before[x][y + 1] - 1))
							After[x][y + 1] = Before[x][y];
						if(Before[x][y] == (Before[x][y - 1] - 1))
							After[x][y - 1] = Before[x][y];
						if(Before[x][y] == (Before[x + 1][y] - 1))
							After[x + 1][y] = Before[x][y];
						if(Before[x][y] == (Before[x - 1][y] - 1))
							After[x - 1][y] = Before[x][y];
					}else{
						if(Before[x][y + 1] == 0)
							After[x][y + 1] = Before[x][y];
						if(Before[x][y - 1] == 0)
							After[x][y - 1] = Before[x][y];
						if(Before[x + 1][y] == 0)
							After[x + 1][y] = Before[x][y];
						if(Before[x - 1][y] == 0)
							After[x - 1][y] = Before[x][y];
					}
				}
			}

			for(x = 1 ; x <= R ; x++)
				for(y = 1 ; y <= C ; y++)
					Before[x][y] = After[x][y];

		}


		/* Imprime Respuesta */
		for(x = 1 ; x <= R ; x++){
			for(y = 1 ; y <= C ; y++){
				if(y < C){
					printf("%d ", Before[x][y]);
				}else{
					printf("%d", Before[x][y]);
				}
			}
			printf("\n");
		}

	}

	return 0;
}
