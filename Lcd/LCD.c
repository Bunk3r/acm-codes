#include<stdio.h>

int main(){
	int s = 0;
	char n[9];
	int veces = 0;
	void PrintTop(char numero[], int s);
	void PrintTopCenter(char numero[], int s);
	void PrintCenter(char numero[], int s);
	void PrintBottomCenter(char numero[], int s);
	void PrintBottom(char numero[], int s);

	while(scanf("%d %s", &s, &n) != EOF){
		if(s == 0 && n[0] == '0')
			break;
		PrintTop(n, s);
		PrintTopCenter(n, s);
		PrintCenter(n, s);
		PrintBottomCenter(n, s);
		PrintBottom(n, s);
		printf("\n");
		}
	return 0;
}

void PrintTop(char numero[], int s){
	int x;
	int y;
	for(x = 0;x<10;x++){
		if(numero[x] == '\0')
			break;
		switch(numero[x]){
		case '1':
		case '4':
				for(y = 0; y < (s + 2) ; y++){
					printf(" ");
				}
				break;
		default:
				printf(" ");
				for(y = 0; y < s ; y++){
					printf("-");
				}
				printf(" ");
		}
		if(numero[x + 1] != '\0')
		printf(" ");
	}
	printf("\n");
	fflush(stdout);
}

void PrintBottom(char numero[], int s){
	int x;
		int y;
		for(x = 0;x<10;x++){
			if(numero[x] == '\0')
				break;
			switch(numero[x]){
			case '1':
			case '4':
			case '7':
					for(y = 0; y < (s + 2) ; y++){
						printf(" ");
					}
					break;
			default:
				printf(" ");
				for(y = 0; y < s ; y++){
					printf("-");
				}
				printf(" ");
			}
			if(numero[x + 1] != '\0')
			printf(" ");
		}
		printf("\n");
		fflush(stdout);
}

void PrintTopCenter(char numero[], int s){
	int x;
	int y;
	int z;
	for(z = 0; z < s ; z++){
			for(x = 0;x<10;x++){
				if(numero[x] == '\0')
					break;
				switch(numero[x]){
				case '1':
				case '2':
				case '3':
				case '7':
						for(y = 0; y < (s + 1) ; y++){
							printf(" ");
						}
						printf("|");
						break;
				case '5':
				case '6':
						printf("|");
						for(y = 0; y < (s + 1) ; y++){
							printf(" ");
						}
						break;
				default:
						printf("|");
						for(y = 0; y < s ; y++){
							printf(" ");
						}
						printf("|");
					}
				if(numero[x + 1] != '\0')
					printf(" ");
				}
				printf("\n");
			}
			fflush(stdout);
}

void PrintBottomCenter(char numero[], int s){
	int x;
		int y;
		int z;
		for(z = 0; z < s ; z++){
				for(x = 0;x<10;x++){
					if(numero[x] == '\0')
						break;
					switch(numero[x]){
					case '2':
							printf("|");
							for(y = 0; y < (s + 1) ; y++){
								printf(" ");
							}
							break;
					case '8':
					case '6':
					case '0':
							printf("|");
							for(y = 0; y < s ; y++){
								printf(" ");
							}
							printf("|");
							break;
					default:
							for(y = 0; y < (s + 1) ; y++){
								printf(" ");
							}
							printf("|");
						}
					if(numero[x + 1] != '\0')
						printf(" ");
					}
					printf("\n");
				}
				fflush(stdout);
}

void PrintCenter(char numero[], int s){
	int x;
		int y;
		for(x = 0;x<10;x++){
			if(numero[x] == '\0')
				break;
			switch(numero[x]){
			case '1':
			case '7':
			case '0':
					for(y = 0; y < (s + 2) ; y++){
						printf(" ");
					}
					break;
			default:
					printf(" ");
					for(y = 0; y < s ; y++){
						printf("-");
					}
					printf(" ");
			}
			if(numero[x + 1] != '\0')
			printf(" ");
		}
		printf("\n");
		fflush(stdout);
	}
