package maquinasturing;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        boolean Continuar = true;
        String Cadena = "";
        String CadenaTop = "";
        String CadenaMid = "";
        String CadenaBot = "";
        String TuringTop = "";
        String TuringMid = "";
        String TuringBot = "";
        int n = 0;
        int t = -1;
        int contador = 0;

        System.out.print("\t\t\tMaquinas de Turing\n");
        System.out.print("\t\t\t WW | W E (a,b,c)*\n\n");
        System.out.print("\t\tIngrese la cadena a Evaluar...\n\t=> ");

        Scanner Lee = new Scanner(System.in);

        Cadena = Lee.nextLine();

        n = Cadena.length();

        System.out.print("\n\n\n");

        char Turing[] = new char[n];

        for(int x = 0; x < n;x++){
            if((Cadena.charAt(x) == 'a') || (Cadena.charAt(x) == 'b') || (Cadena.charAt(x) == 'c')){
                    Turing[x] = '#';
                    TuringTop = "____" + TuringTop;
                    CadenaMid = CadenaMid + "| " + Cadena.charAt(x) + " ";
                    CadenaTop = "____" + CadenaTop;

                    if(x == 0){
                        TuringBot = "  ^ ";
                        CadenaBot = "  ^ ";
                    }else{
                        TuringBot = TuringBot + "____";
                        CadenaBot = CadenaBot + "____";
                    }
            }else{
                System.out.print("\n\n\n\t\tLa cadena solo puede contener a, b, c\n\t\tNo se permite otros caracteres..\n\n");
                Continuar = false;
                break;
            }

        }

        if(Continuar){

        TuringMid = TuringMid + "|";
        CadenaMid = CadenaMid + "|\n";


        Pinta(CadenaTop,CadenaMid,CadenaBot,TuringTop,Turing,TuringBot,n);

/*--------------------------------------------------------------------------------------------------------------------------------------------*/

        for(int x = 1 ; x < n; x++){

                    CadenaBot = "";


                    for(int y = 0 ; y < x ; y++){
                        CadenaBot = CadenaBot + "____";
                    }

                    CadenaBot = CadenaBot + "  ^ ";

                    for(int y = x ; y < (n - 1) ; y++){
                        CadenaBot = CadenaBot + "____";
                    }


                    if((x%2) != 0){
                        t++;
                        TuringBot = "";
                        Turing[((x - 1) / 2)] = 'X';
                        for(int y = 0; y < t ;y++){
                            TuringBot = TuringBot + "____";
                        }

                            TuringBot = TuringBot + "  ^ ";

                        for(int y = ((x - 1) / 2); y < (n - 1);y++){
                            TuringBot = TuringBot + "____";
                        }
                    }

                    Pinta(CadenaTop,CadenaMid,CadenaBot,TuringTop,Turing,TuringBot,n);

                     if ((x == (n - 1)) && (x%2 == 0)){
                            System.out.print("\n\n\n\t\tLa cadena ingresada no puede ser la esperada\n\t\tFavor de ingresar otra cadena...\n\n");
                            Continuar = false;
                            break;
                    }
        
        }

/*--------------------------------------------------------------------------------------------------------------------------------------------*/

        if(Continuar){

                for(int x = (n - 1) ; x > t ; x--){

                    CadenaBot = "";
                    TuringBot = "";

                            for(int y = 0 ; y < x ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                            CadenaBot = CadenaBot + "  ^ ";

                            for(int y = x ; y < (n - 1) ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                    Turing[t - contador] = Cadena.charAt(x);

                                for(int y = 0; y < (t - contador);y++){
                                    TuringBot = TuringBot + "____";
                                }

                                    TuringBot = TuringBot + "  ^ ";

                                for(int y = (t - contador); y < (n - 1);y++){
                                    TuringBot = TuringBot + "____";
                                }


                    Pinta(CadenaTop,CadenaMid,CadenaBot,TuringTop,Turing,TuringBot,n);
                    contador++;
                }

        /*--------------------------------------------------------------------------------------------------------------------------------------------*/

                for(int x = 0 ; x < (t + 1) ; x++){

                    CadenaBot = "";
                    TuringBot = "";

                    if(Cadena.charAt(x) == Turing[x]){

                        for(int y = 0 ; y < x ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                            CadenaBot = CadenaBot + "  ^ ";

                            for(int y = x ; y < (n - 1) ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                                for(int y = 0; y < x ;y++){
                                    TuringBot = TuringBot + "____";
                                }

                                    TuringBot = TuringBot + "  ^ ";

                                for(int y = x ; y < (n - 1);y++){
                                    TuringBot = TuringBot + "____";
                                }

                    }else{

                        Continuar = false;

                        for(int y = 0 ; y < x ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                            CadenaBot = CadenaBot + "  ^ ";

                            for(int y = x ; y < (n - 1) ; y++){
                                CadenaBot = CadenaBot + "____";
                            }

                            Turing[x] = 'X';

                                for(int y = 0; y < x ;y++){
                                    TuringBot = TuringBot + "____";
                                }

                                    TuringBot = TuringBot + "  ^ ";

                                for(int y = x; y < (n - 1);y++){
                                    TuringBot = TuringBot + "____";
                                }

                    }

                    Pinta(CadenaTop,CadenaMid,CadenaBot,TuringTop,Turing,TuringBot,n);

                }

        }
/*--------------------------------------------------------------------------------------------------------------------------------------------*/

        if(Continuar){

            System.out.print("\n\n\n\n\n\n\n\t\tLa cadena ingresada es VALIDA\n\n\n\n\n\n\n\n");

        }else{

            System.out.print("\n\n\n\n\n\n\n\t\tLa cadena ingresada es INVALIDA\n\n\n\n\n\n\n\n");

        }

        }

    }

    public static void Pinta(String CadenaTop, String CadenaMid, String CadenaBot, String TuringTop, char[] Turing, String TuringBot,int n){
        String TuringMid = "";
        try{
            for(int x = 0; x < n; x++){
                TuringMid = TuringMid + "| " + Turing[x] + " ";
            }
            TuringMid = TuringMid + " |";
            System.out.print("\n\n\n\n\n\t\t\t***** Maquina de Turing *****\n\n\t" + CadenaTop + "\n\t" + CadenaMid + "\t" + CadenaBot + "\n\n\n\t");
            System.out.print(TuringTop + "\n\t" + TuringMid + "\n\t" + TuringBot + "\n\n\n\n\n\n\n\n");
            Thread.sleep(2000);
        }catch(InterruptedException ie){};
    }

}
