#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

long fibonacci[46] = {1, 2};

long numeros[200];
char letras[200];
char respuesta[46];
char texto[200];
int max;

void fibo(){
	int x = 2;
	for( ; x < 46 ; x++){
		fibonacci[x] = fibonacci[x - 1] + fibonacci[x - 2];
	}
}

void valida(long num){
	int x = 0;
	for(x = 0 ; x < 46 ; x++){
		if(num == fibonacci[x]){
			if(max <= x){
				max = x;
				break;
			}
		}
	}
}

int main(){

	int N, M, x, y, caso, maxLetters;

	freopen("input.in", "r", stdin);

	fibo();

	scanf("%d", &N);

	for(caso = 0 ; caso < N ; caso++){

		memset(numeros, 0, sizeof(numeros));
		memset(respuesta, ' ', sizeof(respuesta));
		memset(letras, 0, sizeof(letras));
		max = maxLetters = 0;

		scanf("%d", &M);
		for(x = 0 ; x < M ; x++){
			scanf("%ld", &numeros[x]);
			valida(numeros[x]);
		}
		getchar();

		gets(texto);

		for(y = maxLetters = 0 ; texto[y] ; y++){
			if(isupper(texto[y])){
				letras[maxLetters++] = texto[y];
			}
		}

		for(x = 0 ; x < maxLetters ; x++){
			for(y = 0 ; y < 45 ; y++){
				if(numeros[x] == fibonacci[y] && letras[x]){
					respuesta[y] = letras[x];
				}
			}
		}

		respuesta[max + 1] = '\0';

		printf("%s\n", respuesta);
	}

	return 0;
}
