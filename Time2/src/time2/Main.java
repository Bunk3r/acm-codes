package time2;
public class Main {
    public static void main(String[] args){
        Time reloj = new Time(80000);
        System.out.println(reloj.getHoraUniversal());
        System.out.println(reloj.getHora());
        System.out.println(reloj.getSegundos());
        reloj.ticTac();
        System.out.println(reloj.getHora());
        reloj.incrimentarMinuto();
        System.out.println(reloj.getHora());
        reloj.incrimentarHora();
        System.out.println(reloj.getHora());
        reloj.incrimentarDia();
        System.out.println(reloj.getHora());
    }
}
