package time2;
public class Time {
    private int segundos;

    public Time(int seg){
        segundos = seg;
    }

    public Time(){
        segundos = 0;
    }

    public int getSegundos() {
        return segundos;
    }
    
    public void setInicioDia(){
            segundos = segundos % 86400;
    }

    public String getHoraUniversal(){
        int seg = segundos % 60;
        int min = (segundos / 60) % 60;
        int hrs = (segundos / 3600) % 24;
        return hrs + ":" + min + ":" + seg;
    }

    public String getHora(){
        int seg = segundos % 60;
        int min = (segundos / 60) % 60;
        int hrs = (segundos / 3600) % 24;
        if(hrs >= 12)
            return hrs + ":" + min + ":" + seg + " PM";
        else
            return hrs + ":" + min + ":" + seg + " AM";
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public void ticTac(){
        segundos++;
    }

    public void incrimentarMinuto(){
        segundos += 60 - (segundos % 60);
    }

    public void incrimentarHora(){
        segundos += 3600 - (segundos % 3600);
    }

    public void incrimentarDia(){
        segundos = 0;
    }
}