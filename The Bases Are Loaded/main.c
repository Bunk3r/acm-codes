#include<stdio.h>
#include<string.h>

int minBase(char x[]){
	int base = 2, y, aux;
	for(y = 0 ; x[y] ; y++){
            aux = x[y];
                if(aux > 47 && aux < 58)
                    aux -= 48;
                else
                    aux -= 55;
		if(base < aux + 1)
			base = aux + 1;
	}
	return base;
}

unsigned long long poder(int base, unsigned int potencia){
	unsigned long long res;
	if(potencia == 0)
		return 1;
	else if(potencia == 1)
		return base;
	else if(potencia & 1){
		res = poder(base, (potencia - 1)>>1);
		return res * res * base;
	}
	else{
		res = poder(base, potencia>>1);
		return res * res;
	}
	return res;
}

unsigned long long Cambiabase(char num[], int base){
	unsigned long long res = 0;
	int x, y, aux;
	x = strlen(num) - 1;
	for(y = 0; x >= 0 ; y++, x--){
            aux = num[y];
            if(aux > 47 && aux < 58)
		aux -= 48;
            else
		aux -= 55;
		res += aux * poder(base, x);
        }
	return res;
}

int main(){

	char num3[100], resBase[100];
	int num1, num2, min, x, aux;
        unsigned long long res;

        freopen("input.in", "r", stdin);

        while(scanf("%d%d%s", &num1, &num2, num3) != EOF){
		min = minBase(num3);
                if(min > num1){
                    printf("%s is an illegal base %d number\n", num3, num1);
                }
                else{
                    res = Cambiabase(num3, num1);
                    resBase[99] = '\0';
                    x = 98;
                    while(res >= num2){
                        aux = res % num2;
                        if(aux > 9)
                            resBase[x] = aux + 55;
                        else
                            resBase[x] = aux + 48;
                        x--;
                        res /= num2;
                    }
                    if(res > 9)
                        resBase[x] = res + 55;
                    else
                        resBase[x] = res + 48;
                    printf("%s base %d = %s base %d\n", num3, num1, &resBase[x], num2);
		}
	}

	return 0;
}
