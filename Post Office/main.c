#include<stdio.h>

int main(){

	double tick, len, wid, val1, val2, val3;

	freopen("input.in", "r", stdin);

	while(scanf("%lf %lf %lf", &val1, &val2, &val3) && (val1 || val2 || val3)){

		if(val1 > val2 && val1 > val3){
			len = val1;
			if(val2 > val3){
				tick = val3;
				wid = val2;
			}
			else{
				wid = val3;
				tick = val2;
			}
		}
		else if(val2 > val1 && val2 > val3){
			len = val2;
			if(val1 > val3){
				tick = val3;
				wid = val1;
			}
			else{
				wid = val3;
				tick = val1;
			}
		}
		else{
			len = val3;
			if(val2 > val1){
				tick = val1;
				wid = val2;
			}
			else{
				wid = val1;
				tick = val2;
			}
		}

		if(len < 125 || wid < 90 || tick < 0.25){
			printf("not mailable\n");
		}
		else if(len <= 290 && wid <= 155 && tick <= 7){
			printf("letter\n");
		}
		else if(len <= 380 && wid <= 300 && tick <= 50){
			printf("packet\n");
		}
		else if(len + tick + tick + wid + wid <= 2100){
			printf("parcel\n");
		}
		else{
			printf("not mailable\n");
		}

	}

	return 0;
}
