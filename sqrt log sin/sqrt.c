#include<stdio.h>
#include<math.h>
#include<string.h>

#define MAX 1000000

long arr[MAX];

long calcular(long i){
	if(arr[i] < 0){
		if(i){
			long x = i - sqrt(i);
			long y = log(i);
			long z = sin(i) * sin(i) * i;
			return arr[i] = (calcular(x) + calcular(y) + calcular(z))%1000000;
		}
		else
			return arr[i] = 1;
	}
	else
		return arr[i];
}

int main(){
	long i;
	memset(arr, -1, sizeof(arr));
	while(scanf("%ld", &i) && i >= 0)
		if(i)
			printf("%ld\n", calcular(i));
		else
			printf("1\n");
	return 0;
}
