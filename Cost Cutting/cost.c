#include<stdio.h>
int main(){
	int x, y, z, n, caso = 1;
	scanf("%d", &n);
	while(n--){
		scanf("%d %d %d", &x, &y, &z);
		if(x >= y && y >= z)
			printf("Case %d: %d\n",caso++, y);
		else if(x >= z && z >= y)
			printf("Case %d: %d\n",caso++, z);
		else if(y >=x && x >= z)
			printf("Case %d: %d\n",caso++, x);
		else if(y >= z && z >= x)
			printf("Case %d: %d\n",caso++, z);
		else if(z >= y && y >= x)
			printf("Case %d: %d\n",caso++, y);
		else
			printf("Case %d: %d\n",caso++, x);
	}
	return 0;
}
