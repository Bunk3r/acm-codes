#include<stdio.h>
int main(){
	int N, m, M, Dec, Hex, Unos[] = {0,1,1,2,1,2,2,3,1,2};
	scanf("%d", &N);
	while(N--){
		Dec = Hex = 0;
		scanf("%d", &M);
		m = M;
		while(m)
			if(m&1){
				Dec++;
				m >>= 1;
			}
			else
				m >>= 1;
		while(M){
			m = M %10;
			M /= 10;
			Hex += Unos[m];
		}
		printf("%d %d\n", Dec, Hex);
	}
	return 0;
}
