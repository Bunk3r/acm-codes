#include<stdio.h>
#include<math.h>

int main(){
	double x, z;
	const double y = log(2.0);
	unsigned int n;
	int L;
	while(scanf("%u", &n) && n != 0){
		z = log(n);
		L = ceil(z - y);
		x = 1 - exp(z - L);
		printf("%d %.8f\n",L, x);
	}
	return 0;
}
