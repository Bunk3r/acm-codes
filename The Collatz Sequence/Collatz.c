#include<stdio.h>
int main(){
	int caso = 1;
	int terminos;
	long long A, L, X;
	while(scanf("%lld %lld", &A, &L) && A >= 0){
		terminos = 0;
		X = A;
		while(A <= L){
			if(A&1)
				if(A != 1)
					A = (3 * A) + 1;
				else
					break;
			else
				A >>= 1;
			terminos++;
		}
		printf("Case %d: A = %lld, limit = %lld, number of terms = %d\n",caso++,X,L,A == 1 ? terminos + 1 : terminos);
	}
	return 0;
}
