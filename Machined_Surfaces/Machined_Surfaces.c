#include<stdio.h>
int main(){
	unsigned int N = 0;
	char Image[30];
	int Spaces;
	int totalSpaces;
	int x;
	int y;
	int minimo;
	while(scanf("%u", &N) == 1 && N != 0){
		totalSpaces = 0;
		minimo = 25;
		for(x = 0 ; x < N ; x++){
			do{
				fgets(Image, 30, stdin);
			}while(Image[0] == '\n');
			Spaces = 0;
			for(y = 0; y < 25 ; y++){
				if(Image[y] != 'X'){
					Spaces++;
				}
			}
			if(minimo > Spaces)
				minimo = Spaces;
			totalSpaces += Spaces;
		}
		printf("%d\n", totalSpaces - (N * minimo));
	}
	return 0;
}
