#include<stdio.h>
#include<string.h>
int main(){
	unsigned int caso = 1,p,q,aux;
	unsigned long long total;
	char *apuntador;
	int unos[] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8};
	freopen("input.in", "r", stdin);
	while(scanf("%u %u", &p, &q) && !(p == 0 && q == 0)){
		for(aux = p, total = 0 ; aux <= q ; aux++){
			apuntador = &aux;
			total += unos[*apuntador];
			apuntador++;
			total += unos[*apuntador];
			apuntador++;
			total += unos[*apuntador];
			apuntador++;
			total += unos[*apuntador];
		}
		if(p == q)
			total /= 2;
		printf("Case %u: %u\n",caso++, total);
	}
	return 0;
}
