package integer;
import java.math.BigInteger;
import java.io.*;
public class Big {
	public static void main(String[] args) {
		BigInteger total = BigInteger.ZERO;
		BigInteger valor;
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		try {
			do{
			    valor = new BigInteger(stdin.readLine());
				if(valor.equals(BigInteger.ZERO))
					break;
				else 
					total = total.add(valor);
			}while(true);
			System.out.print(total);
		} 
		catch(Exception e) { 
		}
	}
}