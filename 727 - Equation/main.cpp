#include <stdio.h>
#include <stack>

using namespace std;

int main(){

    freopen("input.in", "r", stdin);

    char val[256] = {};
    val['*'] = 10;
    val['/'] = 10;
    val['+'] = 9;
    val['-'] = 9;
    val['('] = -1;
    val[')'] = -1;

    stack<char> s;
    char current;
    int t, c;
    int i, j;

    char expr[1000];
    char output[1000];
    char buffer[10];

    scanf("%d\n\n", &t);
    for(c = 0; c < t; c++){
        if(c)
            printf("\n");

        //Read expression
        for(i = 0; fgets(buffer, 10, stdin) != NULL && buffer[0] != '\n'; i++)expr[i] = buffer[0];
        expr[i] = 0;

        for(j = i = 0; expr[i]; i++){
            if(expr[i] >= '0' && expr[i] <= '9')
                output[j++] = expr[i];
            else if (expr[i] == ')'){
                while(!s.empty()){
                    if(s.top() == '('){
                       s.pop();
                       break;
                    }
                    else{
                        output[j++] = s.top();
                        s.pop();
                    }
                }
            }
            else{
                if(s.empty() || expr[i] == '(')
                    s.push(expr[i]);
                else{
                    while(!s.empty() && val[expr[i]] <= val[s.top()]){
                        current = s.top();
                        s.pop();
                        output[j++] = current;
                    }
                    s.push(expr[i]);
                }
            }
        }
        while(!s.empty()){
            output[j++] = s.top();
            s.pop();
        }
        output[j] = 0;
        printf("%s\n", output);
    }

    return 0;
}
