#include<stdio.h>
#include<string.h>

int minBase(char x[]){
	int base = 2, y, aux;
	for(y = 0 ; x[y] ; y++){
            aux = x[y];
                if(aux > 47 && aux < 58)
                    aux -= 48;
                else
                    aux -= 55;
		if(base < aux + 1)
			base = aux + 1;
	}
	return base;
}

unsigned long long poder(int base, unsigned int potencia){
	unsigned long long res;
	if(potencia == 0)
		return 1;
	else if(potencia == 1)
		return base;
	else if(potencia & 1){
		res = poder(base, (potencia - 1)>>1);
		return res * res * base;
	}
	else{
		res = poder(base, potencia>>1);
		return res * res;
	}
	return res;
}

unsigned long long Cambiabase(char num[], int base){
	unsigned long long res = 0;
	int x, y, aux;
	x = strlen(num) - 1;
	for(y = 0; x >= 0 ; y++, x--){
            aux = num[y];
            if(aux > 47 && aux < 58)
		aux -= 48;
            else
		aux -= 55;
		res += aux * poder(base, x);
        }
	return res;
}

int main(){

	char num1[100], num2[100];
	int min1, aux,  min2, flag;

	while(scanf("%s %s", num1, num2) != EOF){
		min1 = minBase(num1);
		min2 = minBase(num2);
		for(flag = 1; min1 <= 36 && flag ; min1++){
			for(aux = min2 ; aux <= 36 ; aux++){
				if(Cambiabase(num1, min1) == Cambiabase(num2, aux)){
					printf("%s (base %d) = %s (base %d)\n", num1, min1, num2, aux);
					flag = 0;
					break;
				}
			}
		}
		if(flag)
			printf("%s is not equal to %s in any base 2..36\n", num1, num2);
	}

	return 0;
}
