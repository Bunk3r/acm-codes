import java.io.*;
import java.util.Scanner;
import java.math.BigInteger;

class Main
{
    public static void main (String args[]) 
    {
        Main myWork = new Main(); 
        myWork.Begin();           
    }

    void Begin()
    {
        String valorEntrada, operador;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Scanner entrada;
        try{
            do{
                valorEntrada = stdin.readLine();
                if(valorEntrada == null)
                    break;
                else{
                    entrada = new Scanner(valorEntrada);
                    entrada.useDelimiter("\\s+");
                    BigInteger valor1 = new BigInteger(entrada.next());
                    operador = entrada.next();
                    BigInteger valor2 = new BigInteger(entrada.next());
                    if(operador.equals("%")){
                    	System.out.println(valor1.remainder(valor2));
                    }
                    else{
                    	System.out.println(valor1.divide(valor2));
                    }
                }
            }while(true);
        }
        catch (IOException e) {}
    }
}