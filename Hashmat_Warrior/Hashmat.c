#include<stdio.h>
int main(){
	double HashArmy, EnemyArmy;
	while(scanf("%lf %lf", &HashArmy, &EnemyArmy) != EOF){
		printf("%.0lf\n", (EnemyArmy - HashArmy) > 0 ? EnemyArmy - HashArmy : HashArmy - EnemyArmy);
	}
	return 0;
}
