#include<stdio.h>
#include<string.h>

int main(){

	int pages, first, last, totalPagesPrinted;
	char pagesPrinted[1100], next;

	freopen("input.in", "r", stdin);

	while(scanf("%d", &pages) && pages){

		memset(pagesPrinted, 0, 1100);
		totalPagesPrinted = 0;

		do{

			scanf("%d%c", &first, &next);

			if(next == '-'){
				scanf("%d%c", &last, &next);
			}
			else{
				last = first;
			}

			for( ; first <= last && first <= pages ; first++){
				pagesPrinted[first]++;
			}

		}while(next != '\n');

		for(first = 1 ; first <= pages ; first++){
			if(pagesPrinted[first]){
				totalPagesPrinted++;
			}
		}

		printf("%d\n", totalPagesPrinted);

	}

	return 0;
}
