#include<stdio.h>
#include<string.h>

int main(){

	int Cases, Case, x, pos;
	char DNA[50];
	double valores[50], total;

	freopen("input.in", "r", stdin);

	scanf("%d\n", &Cases);

	valores[0] = 1;
	for(x = 1 ; x <= 30 ; x++)
		valores[x] = valores[x - 1] * 4;

	for(Case = 1 ; Case <= Cases ; Case++){

		gets(DNA);
		total = 0;

		for(x = 0, pos = strlen(DNA) - 1 ; DNA[x] ; x++, pos--){

			switch(DNA[x]){
			case 'C':
				total += valores[pos];break;
			case 'G':
				total += valores[pos] * 2;break;
			case 'T':
				total += valores[pos] * 3;break;
			}

		}

		printf("Case %d: (%d:%.0lf)\n",Case, (int)strlen(DNA), total);

	}

	return 0;
}
