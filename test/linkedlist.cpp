#include<cstdio>
#include<list>
using namespace std;

int main(){

	list<int> lista;
	list<int>::iterator puntero;

	for(int x = 1 ; x < 20 ; x++)
		lista.push_front(x);

	puntero = lista.begin();

	while(!lista.empty())
		printf("%d ", *puntero++);

	puntero = lista.begin();
	puntero++;
	lista.remove(*puntero);
	puntero = lista.begin();

	while(!lista.empty())
			printf("%d ", *puntero++);

	return 0;
}
