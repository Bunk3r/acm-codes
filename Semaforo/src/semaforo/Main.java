package semaforo;
import java.util.EnumSet;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        for(LuzSemaforo sem : LuzSemaforo.values()){
            sem.setDuracion(entrada.nextInt());
            System.out.println("Color: " + sem + "\tDuracion: " + sem.getDuracion());
        }

        for(LuzSemaforo sem : EnumSet.range(LuzSemaforo.VERDE, LuzSemaforo.AMARILLO)){
            System.out.println("Color: " + sem + "\tDuracion: " + sem.getDuracion());
        }
    }
}
