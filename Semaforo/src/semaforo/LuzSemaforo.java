package semaforo;
public enum LuzSemaforo {
    ROJO(), VERDE(), AMARILLO();
    private int duracion;
    LuzSemaforo() {
        duracion = 0;
    }
    public int getDuracion() {
        return duracion;
    }
    public void setDuracion(int d){
        duracion = d;
    }
}
