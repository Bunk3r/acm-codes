#include<stdio.h>

int main(){

	int Cases, Case, x;
	char line[100];

	freopen("input.in", "r", stdin);

	scanf("%d\n", &Cases);

	for(Case = 1 ; Case <= Cases ; Case++){

		if(Case > 1)printf("\n");

		while(gets(line)){

			if(line[0] == 0)break;

			for(x = 0 ; line[x] ; x++){

				switch(line[x]){
				case '1':
					printf("I");break;
				case '2':
					printf("Z");break;
				case '3':
					printf("E");break;
				case '4':
					printf("A");break;
				case '5':
					printf("S");break;
				case '6':
					printf("G");break;
				case '7':
					printf("T");break;
				case '8':
					printf("B");break;
				case '9':
					printf("P");break;
				case '0':
					printf("O");break;
				default:
					printf("%c", line[x]);
				}

			}

			printf("\n");

		}

	}

	return 0;
}
