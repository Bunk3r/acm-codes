#include<stdio.h>
#include<stdlib.h>

struct nodo{
	struct nodo * prev;
	char tribe;
	struct nodo * next;
};

typedef struct nodo maid;

maid * begin, * end, * tmp;
char lastKilled = '\0';

int size(){
	int x;
	maid * p = begin;
	for(x = 1 ;  ; x++, p = (* p).next){
		printf("%c -> ", (* p).tribe);
		if(p == begin)
			break;
	}
	printf("end\n");
	return x;
}

void killingSpree(maid * act, int k){
	if(!k && lastKilled){
		if(lastKilled == (* act).tribe){
			(* act).tribe = 'G';
		}
		else{
			(* act).tribe = 'K';
		}
		lastKilled = '\0';
		tmp = (* act).next;
	}
	else if(!k){
		lastKilled = (* act).tribe;
		(* (* act).prev).next = (* act).next;
		(* (* act).next).prev = (* act).prev;
		tmp = (* act).next;
		if(act == begin)
			begin = (* act).next;
		free(act);
	}
	else{
		killingSpree((* act).next, --k);
	}
}


int main(){

	int N, M, K, x, sizeList;

	freopen("input.in", "r", stdin);

	while(scanf("%d %d %d", &N, &M, &K)){

		if(!K && !N && !M)
			break;

		begin = end = tmp = 0;

		for(x = 0 ; x < N ; x++){
			if(!x){
				begin = (maid *)malloc(sizeof(maid));
				(* begin).tribe = 'G';
				end = begin;
				(* end).next = begin;
				(* begin).prev = end;
			}
			else{
				(* end).next = (maid *)malloc(sizeof(maid));
				(*(* end).next).prev = end;
				end = (* end).next;
				(* end).tribe = 'G';
				(* end).next = begin;
				(* begin).prev = end;
			}
		}

		for(x = 0 ; x < M ; x++){
			if(!begin){
				begin = (maid *)malloc(sizeof(maid));
				(* begin).tribe = 'K';
				end = begin;
				(* end).next = begin;
				(* begin).prev = end;
			}
			else{
				(* end).next = (maid *)malloc(sizeof(maid));
				(* (*end).next).prev = end;
				end = (* end).next;
				(* end).tribe = 'K';
				(* end).next = begin;
				(* begin).prev = end;
			}
		}

		tmp = begin;

		while((sizeList = size()) > 1){
			killingSpree(tmp, K - 1);
		}

		printf("%s\n", (* begin).tribe == 'G' ? "Gared" : "keka");

		free(begin);

	}


	return 0;
}
