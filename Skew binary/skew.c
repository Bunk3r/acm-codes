#include<stdio.h>
#include<math.h>
#include<string.h>

int main(){

	char numero[50];
	unsigned int multiplos[] = {0,1,3,7,15,31,63,127,255,511,1023,2047,4095,8191,16383,32767,65535,131071,262143,524287,1048575,2097151,4194303,8388607,16777215,33554431,67108863,134217727,268435455,536870911,1073741823,2147483647};
	int x;
	unsigned int num;
	int maximo;
	unsigned int total;

	do{

		total = 0;

		scanf("%s", &numero[0]);

		maximo = strlen(numero);

		if(!strcmp(numero,"0"))
			break;

		for(x = 0 ; x < maximo ; x++){
			if(numero[x] != '0'){
				if(numero[x] == '1')
					num = multiplos[maximo - x];
				else
					num = 2 * multiplos[maximo - x];
				total += num;
			}
		}

		printf("%u\n", total);

	}while(1);

	return 0;

}
