#include<stdio.h>

int sum(int x){
	int total = 0;
	while(x){
		total += x%10;
		x /= 10;
	}
	return total;
}

int main(){
	int total;
	char num;
	freopen("input.in", "r", stdin);
	while(1){
		scanf("%c", &num);
		if(num == '0')
			break;
		total = num - 48;
		while((num = getchar()) != '\n')
			total += num - 48;
		while(total > 9)
			total = sum(total);
		printf("%d\n", total);
	}
	return 0;
}
