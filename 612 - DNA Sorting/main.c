#include<stdio.h>
#include<stdlib.h>

typedef struct{
	char dna[51];
	int unsorted;
	int order;
}DNA;

int unsortedAmount(char * dna, int length){
	int unsorted, tmpX, tmpY;
	for(tmpX = unsorted = 0 ; tmpX < length ; tmpX++){
		for(tmpY = tmpX + 1 ; tmpY < length ; tmpY++){
			if(dna[tmpX] > dna[tmpY])
				unsorted++;
		}
	}
	return unsorted;
}

int cmpDna(const void * A, const void * B){
	if((*(DNA *)A).unsorted == (*(DNA *)B).unsorted){
		return (*(DNA *)A).order - (*(DNA *)B).order;
	}
	return (*(DNA *)A).unsorted - (*(DNA *)B).unsorted;
}

int main(){

	DNA dnas[100];

	freopen("input.in", "r", stdin);

	int M, n, m, linea, caso = 0;
	scanf("%d", &M);

	for( ; caso < M ; caso++){
		if(caso)
			printf("\n");
		scanf("%d%d\n",&n, &m);
		for(linea = 0 ; linea < m ; linea++){
			scanf("%s", dnas[linea].dna);
			dnas[linea].unsorted = unsortedAmount(dnas[linea].dna, n);
			dnas[linea].order = linea;
		}
		qsort(dnas, m, sizeof(DNA), cmpDna);
		for(linea = 0 ; linea < m ; linea++){
			printf("%s\n", dnas[linea].dna);
		}
	}

	return 0;
}
