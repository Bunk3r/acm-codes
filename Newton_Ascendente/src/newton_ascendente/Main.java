package newton_ascendente;
import java.util.Scanner;

/**
 *
 * @author Carlos Gamez Sanchez
 */
public class Main {

    public static void main(String[] args) {

        int x = 0;
        int Repetir = 0;
        int divisor = 1;
        double delta = 0.0;
        double eval = 0.0;
        double S = 0.0;
        double total = 0.0;
        double subtotal = 0.0;;
        boolean Continuar = true;

        Scanner Lee = new Scanner(System.in);

        do{
            System.out.print("cuantos puntos se van a utilizar...???\n=> ");
            x = Lee.nextInt();
            double arreglofx[] = new double[x];
            double arreglofy[][] = new double[x][x];


                System.out.print("\n\n\n\n\nAcontinuacion ingrese los datos...\n\n");

                for(int contador = 0; contador < x; contador++){
                    System.out.print("Ingrese f(x" + contador + ") => ");
                    arreglofx[contador] = Lee.nextDouble();
                    System.out.print("Ingrese el valor de y" + contador + " => ");
                    arreglofy[contador][0] = Lee.nextDouble();
                }
                
                delta = arreglofx[1] - arreglofx[0];

                for (int contador = 1; contador < x; contador++){
                    if((arreglofx[contador] - arreglofx[contador - 1]) != delta){
                        Continuar = false;
                    }
                }

                if(Continuar){

            for(int columna = 1; columna < x; columna++){
                for(int contador = 0; contador < (x - 1); contador++){

                    arreglofy[contador][columna] = (arreglofy[contador + 1][columna - 1] - arreglofy[contador][columna - 1]) / (arreglofx[contador + 1] - arreglofx[contador]);

                }
            }
            
            System.out.print("\n\n\n\n\n\n\n\nX\t");
            for(int contador = 0; contador < x; contador++){
                System.out.print("f(x"+ contador +")\t");
            }

            System.out.print("\n");
            
            for(int contador = 0; contador < x; contador++){
                System.out.print(arreglofx[contador] + "\t");
                for(int columna = 0; columna < x - contador; columna++){
                    System.out.print(arreglofy[contador][columna] + "\t");
                }
                System.out.print("\n");
            }

            System.out.print("\n\n\nF(x)= " + arreglofy[0][0] + " ");

            for(int columna = 1; columna < x; columna++){

                if(arreglofy[0][columna] != 0.0){
                    if(arreglofy[0][columna] < 0){
                        System.out.print("- " + (arreglofy[0][columna] * -1) + "(S");
                    }else{
                        System.out.print("+ " + arreglofy[0][columna] + "(S");
                    }

                if(columna > 1){
                    for(int contador = 1;contador < columna;contador++){
                        System.out.print("(S-"+contador+")");
                    }
                    divisor = 1;
                    for(int contador = 1;contador < columna + 1;contador++){
                        divisor = divisor * contador;
                    }
                    System.out.print(")/" + divisor);
                }else{
                        System.out.print(") ");
                }

                }
            }
            

            System.out.print("\n\n\nS = (X - " + arreglofx[0] + ")/" + delta + "\n\nQue Numero desea evaluar?\n=> ");
            eval = Lee.nextDouble();

            S = (eval - arreglofx[(x - 1)])/delta;
            total = arreglofy[0][0];

            for(int columna = 1; columna < x ; columna++){
                if(arreglofy[0][columna] != 0){
                subtotal = arreglofy[0][columna] * S;

                for(int contador = 1;contador < columna;contador++){
                        subtotal = subtotal * (S - contador);
                    }
                divisor = 1;
                    for(int contador = 1;contador < columna + 1;contador++){
                        divisor = divisor * contador;
                    }
                    subtotal = subtotal / divisor;

                }

                total = total + subtotal;
               
            }

             System.out.print("\n\nEl resultado es: " + total);

                }
               else{
                    System.out.print("\n\n\n\nEl delta x no es el mismo, favor de verificarlo...");
                    Continuar = true;
                }

            System.out.println("\n\nQuiere volver a ingresar otros datos??(1 = si, 2 = no)\n=> ");
            Repetir = Lee.nextInt();
            if(Repetir != 1){
                Continuar = false;
            }

        }while(Continuar);

    }

}
