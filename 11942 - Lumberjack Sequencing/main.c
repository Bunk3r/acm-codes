#include<stdio.h>

int main(){

	int Cases, Case = 1, x, lumberjacks[10], side;
	char ordered;

	freopen("input.in", "r", stdin);

	printf("Lumberjacks:\n");

	scanf("%d", &Cases);

	for( ; Case <= Cases ; Case++){

		side = 0;
		ordered = 1;

		for(x = 0 ; x < 10 ; x++){
			scanf("%d", &lumberjacks[x]);
		}
		x = 0;
		while(!side){
			if(lumberjacks[x] < lumberjacks[x + 1]){
				side = 1;
			}
			else if(lumberjacks[x] > lumberjacks[x + 1]){
				side = -1;
			}
			x++;
		}

		for( ; x < 9 && ordered ; x++){
			if(lumberjacks[x] < lumberjacks[x + 1] && side == -1){
				ordered = 0;
			}
			else if(lumberjacks[x] > lumberjacks[x + 1] && side == 1){
				ordered = 0;
			}
		}

		if(ordered){
			printf("Ordered\n");
		}
		else{
			printf("Unordered\n");
		}

	}

	return 0;
}
