package turing;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

/*############################################################################################################################################*/

        String C = "";
        String LineaC = "";
        String ContC = "";
        String ArrowC = "";
        String LineaT = "";
        String ArrowT = "";
        boolean Correcto = true;
        int m = 0;
        int n = 0;
        int a = 0;
        int b = 0;

        System.out.print("\t\t@@@@@ Cadenas de Turing @@@@@\n\n");
        System.err.print("\t\t\tA^M B^M+N C^N\n\n");
        System.out.print("\t\tAcontinuacion ingresa la cadena\n\n\t=> ");

        Scanner Scan = new Scanner(System.in);

        C =  Scan.nextLine();
        C = C.toUpperCase();
        int total = C.length();
        char T[] = new char[total];

/*############################################################################################################################################*/

        for(int x = 0; x < total ; x++){
            if((C.charAt(x) == 'A') || (C.charAt(x) == 'B') || (C.charAt(x) == 'C')){

                    T[x] = '-';
                    LineaT = "____" + LineaT;
                    ContC = ContC + "| " + C.charAt(x) + " ";
                    LineaC = "____" + LineaC;
                    if(x == 0){
                        ArrowT = " /\\ ";
                        ArrowC = " /\\ ";
                    }else{
                        ArrowT = ArrowT + "____";
                        ArrowC = ArrowC + "____";
                    }
                }else{
                    System.out.print("\n\n\n\t\tSolo puede haber A , B , C\n\n\n");
                    Correcto = false;
                    break;
                }
            }
        ContC = ContC + "|";

/*############################################################################################################################################*/

        for(int x = 0; x < total ; x++){
            if(C.charAt(x) == 'A'){
                
                m++;
                T[(m - 1)] = 'A';
                ArrowC = "";
                ArrowT = "";
            
                for(int y = 0 ; y < x ; y++){
                        ArrowC = ArrowC + "____";
                        ArrowT = ArrowT + "____";
                    }

                    ArrowC = ArrowC + " /\\ ";
                    ArrowT = ArrowT + " /\\ ";

                    for(int y = x ; y < (total - 1) ; y++){
                        ArrowC = ArrowC + "____";
                        ArrowT = ArrowT + "____";
                    }

                    Turing(LineaC, ContC, ArrowC, LineaT, T, ArrowT, total);
                
            }else{
                Correcto = false;
                break;
            }
        }

        a = m;

/*############################################################################################################################################*/

        for(int x = a; x < total ; x++){
            if(C.charAt(x) == 'B'){
                Correcto = true;

                if(m > 0){
                    m--;
                    T[(m)] = '-';
                    ArrowC = "";
                    ArrowT = "";

                    for(int y = 0 ; y < m ; y++){
                        ArrowT = ArrowT + "____";
                    }
                    for(int y = 0; y < x; y++){
                        ArrowC = ArrowC + "____";
                    }

                    ArrowC = ArrowC + " /\\ ";
                    ArrowT = ArrowT + " /\\ ";

                    for(int y = x + 1 ; y < total ; y++){
                        ArrowC = ArrowC + "____";
                    }
                    for(int y = m + 1 ; y < total ; y++){
                        ArrowT = ArrowT + "____";
                    }
                    
            }else{
                    n++;
                    T[(n - 1)] = 'B';
                    ArrowC = "";
                    ArrowT = "";

                    for(int y = 0 ; y < (n - 1) ; y++){
                        ArrowT = ArrowT + "____";
                    }

                    for(int y = 0; y < x; y++){
                        ArrowC = ArrowC + "____";
                    }

                    ArrowC = ArrowC + " /\\ ";
                    ArrowT = ArrowT + " /\\ ";

                    for(int y = (x + 1) ; y < total ; y++){
                        ArrowC = ArrowC + "____";
                    }
                    for(int y = n; y < total ; y++){
                        ArrowT = ArrowT + "____";
                    }
                }
            }else{
                Correcto = false;
                break;
            }

            Turing(LineaC, ContC, ArrowC, LineaT, T, ArrowT, total);

        }
        
        b = n;

/*############################################################################################################################################*/

            for(int x = a + b; x < total ; x++){
            if(C.charAt(x) == 'C'){
                Correcto = true;

                if(n > 0){
                        n--;
                        T[n] = '-';
                        ArrowC = "";
                        ArrowT = "";

                        for(int y = 0 ; y < n ; y++){
                            ArrowT = ArrowT + "____";
                        }
                        for(int y = 0; y < x; y++){
                            ArrowC = ArrowC + "____";
                        }

                        ArrowC = ArrowC + " /\\ ";
                        ArrowT = ArrowT + " /\\ ";

                        for(int y = x + 1 ; y < total ; y++){
                            ArrowC = ArrowC + "____";
                        }
                        for(int y = n + 1 ; y < total ; y++){
                            ArrowT = ArrowT + "____";
                        }

                    }else{
                        Correcto = false;
                        break;
                    }

                Turing(LineaC, ContC, ArrowC, LineaT, T, ArrowT, total);

                }

            

            }

/*############################################################################################################################################*/

        if(Correcto || (C.length() == 0)){
            System.out.print("\n\n\n\n\t\tEsta Cadena SI es valida  :)\n\n\n\n\n");
        }else{
            System.out.print("\n\n\n\n\t\tEsta Cadena NO es valida :(\n\n\n\n\n\n");
        }


    }

/*############################################################################################################################################*/

    public static void Turing(String LineaC, String ContC, String ArrowC, String LineaT, char[] T, String ArrowT,int total){
        String ContT = "";
        try{
            for(int x = 0; x < total; x++){
                ContT = ContT + "| " + T[x] + " ";
            }
            ContT = ContT + " |";
            System.out.print("\n\n\n\n\n\n\n\n\t" + LineaC + "\n\t" + ContC + "\n\t" + ArrowC + "\n\n\n\t");
            System.out.print(LineaT + "\n\t" + ContT + "\n\t" + ArrowT + "\n\n\n\n\n\n\n\n");
            Thread.sleep(2000);
        }catch(InterruptedException ie){};

/*############################################################################################################################################*/

    }

}
