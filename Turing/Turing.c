#include<stdio.h>
#include<string.h>

#define TLE 10000

typedef struct{
	int qnext;
	int cnext;
	char move;
}properties;

properties estado[1005][2];
int tape[1010];
int input, output;

int main(){
	int M, N, qp, cp, qn, cn, x, pos, st, flag, Ones;
	char mov;
	freopen("input.in", "r", stdin);
	while(scanf("%d %d", &N, &M) && N != 0){
		memset(estado,0,sizeof(estado));
		for(x = 0 ; x < N ; x++){
			scanf("%d %d %d %d %c",&qp, &cp, &qn, &cn, &mov);
			estado[qp][cp].cnext = cn;
			estado[qp][cp].qnext = qn;
			estado[qp][cp].move = mov;
		}
		while(M--){
			memset(tape,0,sizeof(tape));
			scanf("%d %d", &input, &output);
			for(x = 0 ; x < input ; x++)
				tape[x] = 1;
			x = st = pos = Ones = 0;
			flag = 1;
			while(1){
				x++;
				if((mov = estado[st][tape[pos]].move)){
					qn = st;
					st = estado[st][tape[pos]].qnext;
					tape[pos] = estado[qn][tape[pos]].cnext;
					if(mov == 'R')pos++;
					else pos--;
				}
				else
					break;
				if(pos < 0 || pos > 999){
					printf("MLE\n");
					flag = 0;
					break;
				}
				if(x == TLE){
					printf("TLE\n");
					flag = 0;
					break;
				}
			}
			if(flag){
				for(x = 0 ; x < 1000 ; x++)
					if(tape[x] == 1)
						Ones++;
				if(Ones == output)
					printf("AC\n");
				else
					printf("WA\n");
			}
		}
	}
	return 0;
}
