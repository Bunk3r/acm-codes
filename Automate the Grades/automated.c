#include<stdio.h>

int main(){
	int res, n, caso = 1, a,b,c,d,x,y,z;
	scanf("%d", &n);
	while(n--){
		scanf("%d %d %d %d %d %d %d", &a, &b, &c, &d, &x, &y, &z);
		if(x > y)
			if(y > z)
				res = (x + y)/2;
			else
				res = (x + z)/2;
		else
			if(x > z)
				res = (y + x)/2;
			else
				res = (y + z)/2;
		res += a + b + c + d;
		if(res > 89)
			printf("Case %d: A\n", caso++);
		else if(res > 79)
			printf("Case %d: B\n", caso++);
		else if(res > 69)
			printf("Case %d: C\n", caso++);
		else if(res > 59)
			printf("Case %d: D\n", caso++);
		else
			printf("Case %d: F\n", caso++);
	}
	return 0;
}
