#include <iostream>
#include <stack>
using namespace std;

typedef struct{
	unsigned long m;
	unsigned long n;
}Matrix;

stack<Matrix> Operaciones;

Matrix Matriz[30];
Matrix A;
Matrix B;
Matrix C;

unsigned long int total;
char error;



int main()
{
  int i, n;
  char letra,entrada[100];
  void calcular();
  cin >> n;
  for(i = 0 ; i < n ; i++){
	  cin >> letra;
	  cin >> Matriz[letra - 65].m >> Matriz[letra - 65].n;
  }
  while(gets(entrada)){
	  total = 0;
	  error = 0;
	  i = 0;
	  if(entrada[0] == '\0')
		  continue;

	  while(!Operaciones.empty())
		  Operaciones.pop();

		  while(entrada[i] != '\0'){
			  if(entrada[i] == 41){
				  A = Operaciones.top();
				  Operaciones.pop();
				  B = Operaciones.top();
				  Operaciones.pop();
				  C.n = A.n;
				  C.m = B.m;
				  if(A.m == B.n)
				  	total += C.n * B.m * B.n;
				  else{
				  	error = 1;
				  	break;
				  }
				  Operaciones.push(C);
			  }
			  else if(entrada[i] != 40)
				  Operaciones.push(Matriz[entrada[i] - 65]);
			  i++;
		  }
				  if(error)
					  cout << "error" << endl;
				  else
					  cout << total << endl;
			  }
  return 0;
}
