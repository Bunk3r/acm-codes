#include <stdio.h>

int main(){
	int parm, ns, os = 0, x = 0;
	float H, M, h, m, s, dif;
	double dist = 0.0;
	char line[50];
	freopen("input.in", "r", stdin);
	while(fgets(line, 50, stdin) != NULL){
		parm = sscanf(line, "%f:%f:%f %d", &h, &m, &s, &ns);

		if(parm == 4){
			if(x++){
				m += s / 60;
				h += m / 60;
				dif = h - H;
				H = h;
				dist += dif * os;
				os = ns;
			}
			else{
				M = m + (s / 60);
				H = h + (M / 60);
				os = ns;
			}
		}
		else{
			printf("%02.0f:%02.0f:%02.0f ", h, m , s);
			m += s / 60;
			h += m / 60;
			dif = h - H;
			dist += dif * os;
			printf("%.02f km\n", dist);
			H = h;
		}
	}
	return 0;
}
