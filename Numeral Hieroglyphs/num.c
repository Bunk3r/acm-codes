#include<stdio.h>
#include<string.h>

char entrada[550];

int validar(){
	int x, tope, flag = 0;
	for(x = 0, tope = 0 ; entrada[x] ; x++)
		if(entrada[x] >= tope)
			tope = entrada[x];
		else{
			flag++;
			break;
		}

	for(x = 0, tope = 8 ; entrada[x] ; x++)
		if(entrada[x] <= tope)
			tope = entrada[x];
		else{
			flag++;
			break;
		}

	if(flag == 2)
		return 0;
	else
		return 1;
}

int main(){
	unsigned long long total, valores[] = {1,10,100,1000,10000,100000,1000000};
	int N, cuantos[7], x, val;
	char Letra, flag;
	freopen("input.in", "r", stdin);
	scanf("%d\n", &N);
	while(N--){
		memset(cuantos, 0, sizeof(cuantos));
		memset(entrada, 0, sizeof(char)<<7);
		flag = total = x = 0;
		while(scanf("%c", &Letra) && Letra != '\n'){
			switch(Letra){
			case 'B':
				cuantos[0]++;
				val = 1;
				break;
			case 'U':
				cuantos[1]++;
				val = 2;
				break;
			case 'S':
				cuantos[2]++;
				val = 3;
				break;
			case 'P':
				cuantos[3]++;
				val = 4;
				break;
			case 'F':
				cuantos[4]++;
				val = 5;
				break;
			case 'T':
				cuantos[5]++;
				val = 6;
				break;
			case 'M':
				cuantos[6]++;
				val = 7;
				break;
			}
			if(entrada[x]){
				if(entrada[x] != val)
					entrada[++x] = val;
			}
			else
				entrada[x] = val;
		}
		for(x = 0; x < 7 ; x ++)
			if(cuantos[x] < 10)
				total += cuantos[x] * valores[x];
			else{
				flag = 1;
				break;
			}

		if(flag)
			printf("error\n");
		else{
			if(validar())
				printf("%llu\n", total);
			else
				printf("error\n");
		}
	}
	return 0;
}
