#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
	int pos;
	char *Puntero;
}nodo;

int cmp(const void *A, const void *B){
	return (*(nodo *)A).pos - (*(nodo *)B).pos;
}

int main(){
	int cases, c, n, i;
	char line1[100000], line2[100000];
	int v1[100000];
	nodo v2[100000];
	char *p;
	freopen("input.in", "r", stdin);
	scanf("%d\n", &cases);

	for(c = 0; c < cases;){
		if(c++)
			printf("\n");
		scanf("%[^\n]\n", line1);
		scanf("%[^\n]\n", line2);

		n = 0;
		p = strtok(line1, " ");
		while(p != NULL){
			sscanf(p, "%d", v1+n);
			n++;
			p = strtok(NULL, " ");
		}

		n = 0;
		p = strtok(line2, " ");
		while(p != NULL){
			v2[n].pos = v1[n];
			v2[n].Puntero = p;
			p = strtok(NULL, " ");
			n++;
		}

		qsort(v2, n , sizeof(nodo), cmp);

		for(i = 0; i < n; printf("%s\n", v2[i++].Puntero));
	}
	return 0;
}
