#include<stdio.h>
#include<string.h>

char spot[502][502];

int main(){

	int W, H, N, X1, X2, Y1, Y2, addX, addY, x, y, total;

	freopen("input.in", "r", stdin);

	while(scanf("%d %d %d", &W, &H, &N) && W != 0){
		memset(spot, 0, sizeof(spot));
		total = W * H;
		while(N--){
			scanf("%d %d %d %d", &X1, &Y1, &X2, &Y2);
			addX = X2 < X1 ? -1 : 1;
			addY = Y2 < Y1 ? -1 : 1;
			for(x = X1 ; x - X2 != addX ; x += addX)
				for(y = Y1 ; y - Y2 != addY ; y += addY)
					if(spot[x][y] != '@'){
						spot[x][y] = '@';
						total--;
					}
		}
		switch(total){
			case 0:
				printf("There is no empty spots.\n");
				break;
			case 1:
				printf("There is one empty spot.\n");
				break;
			default:
				printf("There are %d empty spots.\n", total);
		}
	}

	return 0;
}
