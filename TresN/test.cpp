#include<stdio.h>

#define MAX(x, y) (x) > (y) ? (x) : (y)
#define SWAP(a, b) {a ^= b; b ^= a; a ^= b;}

#define SIZE 1000000

static unsigned table[SIZE];

unsigned _get_cycle_len(unsigned n, unsigned *cycle_len_counter)
{
if (n 1)
{
if (n & 1)
{
n = 3 * n + 1;
}
else
{
n >>= 1;
}

if (n j)
{
SWAP(i, j);
}

while (i max)
{
max = table[i];
}
}
else
{
*cycle_len_counter = 0; // Must initialize to zero.
table[i] = _get_cycle_len(i, cycle_len_counter); // Get the cycle length of the number.

if (table[i] > max)
{
max = table[i];
}
}

//printf(�table[%u]: %u\n�, i, table[i]);

i++;
}

return max;
}

int main(void)
{
unsigned i = 0, j = 0;

// ### Use this block to Find the max cycle length in a range of number.
while(scanf(�%u %u�, &i, &j) != EOF)
{
printf(�%u %u %u\n�, i, j, _get_max_cycle_len(i, j));
}

return 0;
}
