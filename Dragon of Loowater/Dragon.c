#include<stdio.h>
#include<stdlib.h>

int cmp(const void *A,const void *B){
	return (*(int *)A) - (*(int *)B);
}


int main(){
	int N, M, x, y, arrHeads[30000], arrKnights[30000], total,flag;
	freopen("input.in", "r", stdin);
	while(scanf("%d %d", &N, &M) && N != 0 && M != 0){
		for(x = flag = 0 ; x < N ; x++)
			scanf("%d", &arrHeads[x]);
		for(y = 0 ; y < M ; y++)
			scanf("%d", &arrKnights[y]);
		if(N > M)
			flag = 1;
		else{
			qsort(arrHeads,x,sizeof(int), cmp);
			qsort(arrKnights,y,sizeof(int), cmp);
			for(x = y = total = 0 ; x < N ;x++)
				for(;y <= M ;y++)
					if(y == M)
						flag = 1;
					else{
						if(arrHeads[x] <= arrKnights[y]){
							total += arrKnights[y++];
							break;
						}
					}
		}
		if(flag)
			printf("Loowater is doomed!\n");
		else
			printf("%d\n", total);
	}
	return 0;
}
