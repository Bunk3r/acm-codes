#include<stdio.h>


int main(){

	int N, n, max, total, act, tmp, fifty, sixty, seventy;

	freopen("input.in", "r", stdin);

	while(scanf("%d", &N) && N){

		fifty = sixty = seventy = max =  total = 0;

		for(n = 0 ; n < N ; n++){

			scanf("%d", &act);
			total += act;
			max = max < act ? act : max;

		}

		if(total > 25){
			printf("0 ");
		}
		else{
			for(fifty = 0, tmp = 50 ; tmp >= 0 ; tmp -= max){
				fifty++;
			}
			printf("%d ", fifty);
		}

		if(total > 30){
			printf("0 ");
		}
		else{
			for(sixty = 0, tmp = 60 ; tmp >= 0 ; tmp -= max){
				sixty++;
			}
			printf("%d ", sixty);
		}

		if(total > 35){
			printf("0\n");
		}
		else{
			for(seventy = 0, tmp = 70 ; tmp >= 0 ; tmp -= max){
				seventy++;
			}
			printf("%d\n", seventy);
		}



	}

	return 0;
}
