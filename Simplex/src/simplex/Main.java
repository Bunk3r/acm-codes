
package simplex;
import java.util.Scanner;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;




/**
 * @author Jorge Luis Aguilar Diaz
 */

public class Main {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        Boolean Repetir = true; // Validar si se desea repetir o no el programa.
        String entrada = "";    // Se guarda el valor de texto linea por linea.
        String texto = "";      // Variable donde se guardan los datos del archivo datos.dat.
        int i = 0;      // Columna.
        int j = 0;      // Fila.
        int x = 0;      // La cantidad de columnas de la matriz.
        int y = 0;      // La cantidad de filas de la matriz.
        int w;          // Validar si desea salir o si desea repetir el programa.
        double NumeroMayor = 0.0;    // Sirve para validar la cantidad de espacion necesarios a la hora de imprimir la matriz.
        double NumeroActual = 0.0;   // Sirve para validar la cantidad de espacion necesarios a la hora de imprimir la matriz.
        int Ceros;                  // Sirve para validar la cantidad de espacion necesarios a la hora de imprimir la matriz.
        double pivote = 0.0;        // Variable pivote para obtener la inversa de la Matriz Entrante y tambien para realizar las...
        int pivoteX = 0;      // operaciones del metodo simplex.
        int pivoteY = 0;
        String ubicacion = "C:\\datos.dat";

        System.out.print("\n\n\tpowered by Jorge Luis Aguilar Diaz\n");
        System.out.print("\tMatricula: 1201124\n");
        System.out.print("\tMateria: Investigacion de Operaciones\n");
        System.out.print("\tTema: Metodo Simplex\n\n\n\n");


        while(Repetir){

        // ****************************************************************************
        // Extrae los datos del archivo datos.dat y los ingresa en la variable texto
        // ****************************************************************************

        Scanner Salir = new Scanner(System.in);


        // **************************************************************
        //  Ubicacion del archivo datos.dat por default es C:\datos.dat
        //  *************************************************************


        System.out.print("Cual es la ubicacion del archivo datos.dat ??\n");

        ubicacion = Salir.next();

        File archivo = new File(ubicacion);

        try {
            FileReader Lee = new FileReader(archivo);
            BufferedReader LeeLinea = new BufferedReader(Lee);
            try {

                while( (entrada  = LeeLinea.readLine()) != null){
                    texto = texto + entrada + "  \n";
                }

            }
            catch(java.io.IOException NoHayNada){
                System.out.println("se presento el error: "+ NoHayNada.toString());
            }

        }
        catch(java.io.FileNotFoundException fnfex) {
                System.out.println("se presento el error: "+fnfex.toString());
        }

        Scanner Scan = new Scanner(texto);
        entrada = Scan.nextLine();
        Scanner EntradatamanoArreglo = new Scanner(entrada);
        EntradatamanoArreglo.useDelimiter(",");


                //************************************************************************************************
                // Extrae las dimensiones de la matriz de la variable texto y los asigna a X y Y respectivamente
                //************************************************************************************************
        
                if(EntradatamanoArreglo.hasNext()){

                    x = Integer.valueOf(EntradatamanoArreglo.next().trim());
                }
                else{
                    System.out.println("A ocurrido un error:\n");
                    System.out.println("No se encuentran los datos necesarios para realizar las operaciones!!!");
                    System.out.println("\nFavor de verificar que los datos se encuentren en el archivo datos.dat.\n");
                    break;
                }
                if(EntradatamanoArreglo.hasNext()){
                    y = Integer.valueOf(EntradatamanoArreglo.next().trim());
                }
                else{
                    System.out.println("A ocurrido un error:\n");
                    System.out.println("No se encuentran los datos necesarios para realizar las operaciones!!!");
                    System.out.println("\nFavor de verificar que los datos se encuentren en el archivo datos.dat.\n");
                    break;
                }

                if(x > 99 || y > 99){

                    System.out.println("Las dimensiones del arreglo no pueden sobrepasar de 100 ya sea en X o en Y !!!\n\n");
                    break;

                }

                //**********************************
                // Se crea una matriz de tamano MxN
                //**********************************


                Double Datos[][] = new Double[x][y];
                Double MatrizInversa[][] = new Double[x][y];
                Double MatrizIdentidad[][] = new Double[x][y];
                Double MatrizTableau[][] = new Double[(x + 1)][(y + 1)];


                //*************************************************************************
                // Extrae los datos de la variable texto y los ingresa dentro del arreglo
                //*************************************************************************


                for(j = 0 ; j < y ; j++){

                    entrada = Scan.nextLine();
                    Scanner EntradaDatosArreglo = new Scanner(entrada);
                    EntradaDatosArreglo.useDelimiter(",");

                    for(i = 0 ; i < x ; i++){

                        if(EntradaDatosArreglo.hasNext()){
                            Datos[i][j] = (double)Math.round(10 * Integer.valueOf(EntradaDatosArreglo.next().trim())) / 10;
                            MatrizIdentidad[i][j] =  Datos[i][j];
                            NumeroActual = Datos[i][j];
                            MatrizInversa[i][j] = 0.0;
                            if(i == j){
                                MatrizInversa[i][j] = 1.0;
                            }
                            if(NumeroActual > NumeroMayor){
                                NumeroMayor = NumeroActual;
                            }
                            if(-(NumeroActual) > NumeroMayor){
                                NumeroMayor = NumeroActual;
                            }
                        }
                        else{
                           System.out.println("A ocurrido un error:\n");
                            System.out.println("No se encuentran los datos necesarios para realizar las operaciones!!!");
                            System.out.println("\nFavor de verificar que los datos se encuentren en el archivo datos.dat.");
                            break;
                        }

                    }
                }


                /*  Se consigue la inversa de la matriz proporcionada


                for(int t = 0; t < x ; t++){

                    if(t < y){
                        if(MatrizIdentidad[t][t] != 1){

                            pivote = MatrizIdentidad[t][t];

                            for(i = 0 ; i < x ; i++){

                                MatrizIdentidad[i][t] = MatrizIdentidad[i][t] / pivote;
                                MatrizInversa[i][t] = MatrizInversa[i][t] / pivote;

                            }

                        }
                    }

                    for(j = 0 ; j < y ; j++){

                        if((t != j) & (MatrizIdentidad[t][j] != 0)){
                            
                        pivote = -(MatrizIdentidad[t][j]);

                            for(i = t ; i < x ; i++){
                                    
                                    MatrizIdentidad[i][j] = MatrizIdentidad[i][j] + pivote * (MatrizIdentidad[i][t]);
                                    MatrizInversa[i][j] = MatrizInversa[i][j] + pivote * (MatrizIdentidad[i][t]);
                                    
                                }

                            }

                    }

                }


                 */





                //**************************************
                // Imprime la matriz en forma ordenada
                //**************************************

                System.out.print("Las dimensiones de la matriz ingresada son:\n" + x + " X " + y);
                System.out.println("\n\nEsta es la matriz ingresada en el archivo datos.dat:\n");

                        NumeroActual = 0;

                        while(NumeroMayor >= 10 || NumeroMayor <= -10){

                                NumeroMayor = NumeroMayor/10;
                                NumeroActual++;

                        }

                        NumeroMayor = NumeroActual;
                        NumeroActual = 0;
                        

                        for(j = 0 ; j < y ; j++){

                        System.out.print("(  ");
                    for(i = 0 ; i < x ; i++){
                            Ceros = 0;
                            NumeroActual = Datos[i][j];

                            while(NumeroActual >= 10 || NumeroActual <= -10){
                                NumeroActual = NumeroActual/10;
                                Ceros++;

                            }

                            while(NumeroMayor > Ceros){
                                        System.out.print(" ");
                                        Ceros++;
                            }
                            if(Datos[i][j] < 0){
                                System.out.print(Datos[i][j] + "   ");
                            }
                            else{
                                System.out.print(" " + Datos[i][j] + "   ");
                            }

                        }
                        System.out.print(")\n");

                    }

                        System.out.print("\n\n\n");


                        // Creacion del Tableau para realizar el metodo Simplex


                        entrada = Scan.nextLine();
                        Scanner EntradaDatosVariablesBasicas = new Scanner(entrada);
                        EntradaDatosVariablesBasicas.useDelimiter(",");
                        MatrizTableau[0][y] = 0.0;


                        for(j = 0 ; j < y + 1 ; j++){
                            for(i = 0 ; i < x + 1 ; i++){

                                if(i == 0){
                                    if(j != 0){
                                        if(EntradaDatosVariablesBasicas.hasNext()){
                                            if(j != y){
                                                MatrizTableau[0][j] = (double)Math.round(10 * Integer.valueOf(EntradaDatosVariablesBasicas.next().trim())) / 10;
                                            }
                                        }
                                     }
                                }
                                if(j == 0){

                                    MatrizTableau[i][0] = (double)i;

                                }
                                if((i !=0 ) & (j != 0)){

                                    MatrizTableau[i][j] = Datos[(i - 1)][(j - 1)];

                                }

                            }
                        }


                        while(Repetir){
                            Repetir = true;
                        // Conseguir la fila saliente y la columna entrante


                        pivoteX = 1;
                        NumeroActual = MatrizTableau[1][y];
                        for(i = 2; i < x ; i++){

                            if (MatrizTableau[i][y] > NumeroActual){
                               if(NumeroActual >= 0){
                                pivoteX = 0;
                               }
                            }
                            else if(MatrizTableau[i][y] < NumeroActual){
                                if(MatrizTableau[i][y] < 0){
                                pivoteX = i;
                                NumeroActual = MatrizTableau[i][y];
                                }
                            }
                        }

                        if(pivoteX != 0){

                        pivoteY = 1;
                        NumeroActual = MatrizTableau[x][1] / MatrizTableau[pivoteX][1];
                        for(j = 2; j < y; j++){

                            if (((MatrizTableau[x][j] / MatrizTableau[pivoteX][j]) > NumeroActual)){
                                if(NumeroActual <= 0){
                                    pivoteY = 0;
                                    if((MatrizTableau[x][j] / MatrizTableau[pivoteX][j]) > 0){
                                        pivoteY = j;
                                        NumeroActual = MatrizTableau[x][j] / MatrizTableau[pivoteX][j];
                                    }
                                }
                            }
                            else if((MatrizTableau[x][j] / MatrizTableau[pivoteX][j]) < NumeroActual){
                            if((MatrizTableau[x][j] / MatrizTableau[pivoteX][j]) > 0){
                                    pivoteY = j;
                                    NumeroActual = MatrizTableau[x][j] / MatrizTableau[pivoteX][j];
                                }
                            }

                        }

                        if(pivoteY != 0){
                        
                            pivote = MatrizTableau[pivoteX][pivoteY];

                            System.out.print("Fila Saliente: " + pivoteX + " .......");
                            System.out.print("Columna Entrante: " + pivoteY + " .......");
                            System.out.print("pivote: " + pivote + "\n");

                            for(i = 1; i < x + 1 ;i++ ){

                                MatrizTableau[i][pivoteY] = MatrizTableau[i][pivoteY] / pivote;

                            }

                            for(j = 1; j < (y + 1) ; j++){
                                
                                if(j != pivoteY){

                                    pivote = MatrizTableau[pivoteX][j];
                                    for(i = 1;i < (x + 1);i++){

                                        MatrizTableau[i][j] =  MatrizTableau[i][j] - (pivote * MatrizTableau[i][pivoteY]);
                                    
                                    }

                                }

                            }

                            MatrizTableau[0][pivoteY] = MatrizTableau[pivoteX][0];
                        }

                        else{
                            System.out.print("Debeido a que no hay algun cociente que sea positivo\n");
                            System.out.print("las iteraciones terminan!!\n\n");
                            Repetir = false;
                        }
                         }
                        else{
                            System.out.print("Debeido a que no quedan numeros negativos para maximizar\n");
                            System.out.print("las iteraciones terminan!!\n\n");
                            Repetir = false;
                        }

                        }

                System.out.print("\n\n\n");

                //**************************************
                // Imprime la matriz en forma ordenada
                //**************************************

                System.out.print("Este es el Tableau de la ultima iteracion: \n");

                        NumeroActual = 0;
                        NumeroMayor = 0;
                        for(j = 0 ; j < y + 1 ; j++){

                            for(i = 0 ; i < x + 1 ; i++){

                                if(NumeroMayor < MatrizTableau[i][j]){
                                    NumeroMayor = MatrizTableau[i][j];
                                }
                                if(NumeroMayor < -(MatrizTableau[i][j])){
                                    NumeroMayor = MatrizTableau[i][j];
                                }
                            }
                        }


                        while(NumeroMayor >= 10 || NumeroMayor <= -10){

                                NumeroMayor = NumeroMayor/10;
                                NumeroActual++;

                        }

                        NumeroMayor = NumeroActual;
                        NumeroActual = 0;

                        for(j = 0 ; j < y + 1; j++){
                        if(j == 1){
                            System.out.print("================================================================\n");
                        }
                        System.out.print("(  ");
                    for(i = 0 ; i < x + 1; i++){
                            Ceros = 0;
                            NumeroActual = MatrizTableau[i][j];

                            while(NumeroActual >= 10 || NumeroActual <= -10){
                                NumeroActual = NumeroActual/10;
                                Ceros++;

                            }

                            MatrizTableau[i][j] = (double)(Math.round(10 * MatrizTableau[i][j]))/10;
                            
                            while(NumeroMayor > Ceros){
                                        System.out.print(" ");
                                        Ceros++;
                            }
                            if((i != 0)){
                                if(MatrizTableau[i][j] < 0){
                                    System.out.print(MatrizTableau[i][j] + "   ");
                                }
                                else{
                                    System.out.print(" " + MatrizTableau[i][j] + "   ");
                                }
                            }
                            else{
                                if(j == 0){
                                    System.out.print("X\\Y   | ");
                                }
                                else if(j == y){
                                    System.out.print("  Z   | ");
                                }
                                else{
                                if(MatrizTableau[i][j] < 0){
                                    System.out.print(MatrizTableau[i][j] + "  | ");
                                }
                                else{
                                    System.out.print(" " + MatrizTableau[i][j] + "  | ");
                                }
                                }
                            }

                        }
                        System.out.print(")\n");

                    }

                        System.out.print("\n\n\n");
                        System.out.print("********************************************************************************\n");
                        System.out.print("********************************************************************************\n\n");

                        System.out.print("La respuesta al problema de Maximizacion:\n\n");
                        System.out.print("Z = ");
                        for(i = 0; i < x; i++){
                            if(-(Datos[i][y - 1]) > 0){
                                System.out.print("+" + -(Datos[i][y - 1]) + " X" + (i + 1) + " ");
                            }
                            else if(-(Datos[i][y - 1]) < 0){
                                System.out.print(-(Datos[i][y - 1]) + " X" + (i + 1) + " ");
                            }
                        }
                        System.out.print("\nZ = ");
                       for(j = 1; j < y ; j++){

                           pivoteX = (int)Math.round(MatrizTableau[0][j]);
                           pivoteX = pivoteX - 1;

                           if(-(Datos[pivoteX][y - 1]) > 0){
                           System.out.print("+" + -(Datos[pivoteX][y - 1]) + "( " + MatrizTableau[x][j] + " )" + " ");
                           }
                           else if(-(Datos[pivoteX][y - 1]) < 0){
                               System.out.print(-(Datos[pivoteX][y - 1]) + "( " + MatrizTableau[x][j] + " )" + " ");
                           }

                       }
                        
                        System.out.print("\nZ = " + MatrizTableau[x][y]);
                        System.out.print("\n\n********************************************************************************\n");
                        System.out.print("********************************************************************************");

                           System.out.print("\n\n");
                        
                    do{
                        Repetir = true;
                    System.out.print("Quiere que se vuelva a ejecutar el programa ??\n");
                    System.out.print("1 = SI / 2 = NO\n");
                    w = Salir.nextInt();
                    }while(w != 1 && w != 2);

                    if(w == 2){
                        Repetir = false;
                    }

                }
    }

}



