#include <stdio.h>
#include<string.h>
#define MAX 1000005
int arr[MAX];

int calcula(unsigned long x){
	if(!arr[x]){
		if(x&1)
			arr[x] = calcula(x * 3 + 1) + 1;
		else
			arr[x] = calcula(x>>1) + 1;
	}
	return arr[x];
}

int main(){
	int contaF, conta;
	unsigned long a, z,i;
	memset(arr,0,sizeof(arr));
	arr[1] = 1;
	while (scanf( "%lu %lu", &a, &z ) == 2){
		contaF = 0;
		printf( "%lu %lu ",a,z);
        if(a > z){
        	a ^= z;
            z ^= a;
            a ^= z;
        }
        for(i = a; i <= z; i++){
        	conta = calcula(i);
            if( contaF < conta)
            	contaF = conta;
        }
        printf( "%d\n", contaF );
	}
return 0;
}
