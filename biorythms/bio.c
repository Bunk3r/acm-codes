#include<stdio.h>

int main(){
	int caso = 1,p, e, i,P, E, I,dias, inicio,x, y;
	freopen("input.in", "r", stdin);
	while(scanf("%d %d %d %d", &p,&e,&i,&inicio)){
		if(p < 0)
			break;

		if(p == e && e == i && i == inicio){
			printf("Case %d: the next triple peak occurs in 21252 days.\n", caso++);
			continue;
		}

		P = p % 23;
		E = e % 28;
		I = i % 33;

		if(P == 0 && E == 0 && I == 0){
			printf("Case %d: the next triple peak occurs in %d days.\n", caso++, 21252 - inicio);
			continue;
		}
		dias = inicio;
		inicio -= ((inicio - I) % 33);
		for(x = inicio, y = 0; y <= 21252 ; x += 33, y += 33)
			if((x - P) % 23 == 0 && (x - E)% 28 == 0){
					printf("Case %d: the next triple peak occurs in %d days.\n", caso++, x - dias);
					break;
			}
	}
	return 0;
}
