#include<iostream>
#include<string>
#include <ctype.h>
using namespace std;

typedef struct
{
	string apellido_paterno;
	string apellido_materno;
	string nombre;
	string telefono;
}datos;

datos agenda[30];
datos pivote;

int a;
int op;
int temp;
int c;
void ordenar();
void menu();
void desplegar();
void eliminar();
void buscar();

void menu()
{
	cout<<"\n\n\n\n1.- Ingresar contacto\n";
	cout<<"2.- Buscar contacto\n";
	cout<<"3.- Eliminar contacto\n";
	cout<<"4.- Ordenar contactos\n";
	cout<<"5.- Mostrar contactos\n";
	cout<<"6.- Salir\n";
	cin>>op;
}


void ordenar()
{
	for(int i=0 ; i < a ; i++)
		for(int j=0 ; j < a - 1 ; j++)
			if(agenda[j].apellido_paterno.compare(agenda[j + 1].apellido_paterno) > 0)
			{
				pivote.apellido_paterno = agenda[j].apellido_paterno;
				agenda[j].apellido_paterno = agenda[j + 1].apellido_paterno;
				agenda[j + 1].apellido_paterno = pivote.apellido_paterno;
				pivote.apellido_materno = agenda[j].apellido_materno;
				agenda[j].apellido_materno = agenda[j + 1].apellido_materno;
				agenda[j + 1].apellido_materno = pivote.apellido_materno;
				pivote.nombre = agenda[j].nombre;
				agenda[j].nombre = agenda[j + 1].nombre;
				agenda[j + 1].nombre = pivote.nombre;
				pivote.telefono = agenda[j].telefono;
				agenda[j].telefono = agenda[j + 1].telefono;
				agenda[j + 1].telefono = pivote.telefono;
			}
}


void buscar()
{

	cout << "\n\n\nIngrese el Apellido Paterno que Busca:\n";
	cin >> pivote.apellido_paterno;
	for(int d = 0 ; d < a ; d++)
		if(stricmp(pivote.apellido_paterno.c_str() , agenda[d].apellido_paterno.c_str()) == 0)
		{
			cout << (d + 1) << ".- ";
			cout << agenda[d].apellido_paterno << "\t";
			cout << agenda[d].apellido_materno << "\t";
			cout << agenda[d].nombre << "\t";
			cout << agenda[d].telefono << "\n";
		}

}


void desplegar()
{
	cout << "\n\n\n";
	for(int d = 0 ; d < a ; d++)
	{

		cout << (d + 1) << ".- ";
		cout << agenda[d].apellido_paterno << "\t";
		cout << agenda[d].apellido_materno << "\t";
		cout << agenda[d].nombre << "\t";
		cout << agenda[d].telefono << "\n";

	}
}

void ingresar()
{
	cout << "\n\n\nApellido Paterno: \t";
	cin >> agenda[a].apellido_paterno;
	cout << "\nApellido Materno: \t";
	cin >> agenda[a].apellido_materno;
	cout << "\nNombre: \t";
	cin >> agenda[a].nombre;
	cout << "\nTelefono: \t";
	cin >> agenda[a].telefono;
	a++;
	if(a>20)
		cout << "Agenda llena\n\t";
}

void eliminar()
{
	cout << "\n\n\nIngrese el contacto que desea eliminar\n";
	cin >> c;

	agenda[c - 1].apellido_paterno = "";
	agenda[c - 1].apellido_paterno = "";
	agenda[c - 1].nombre = "";
	agenda[c - 1].telefono = "";

	cout << "\nIngrese nuevo contacto\n\nApellido paterno: \t";
	cin >> agenda[c - 1].apellido_paterno;
	cout << "\nApellido materno: \t";
	cin >> agenda[c - 1].apellido_materno;
	cout << "\nNombre: \t";
	cin >> agenda[c - 1].nombre;
	cout << "\nTelefono: \t";
	cin >> agenda[c - 1].telefono;

}


int main()
{
	a=0;


	do{
		menu();
		switch(op)
		{
			case 1:
				ingresar();
				break;
			case 2:
				buscar();
				break;
			case 3:
				eliminar();
				break;
			case 4:
				ordenar();
				break;
			case 5:
				desplegar();
				break;
		}
	}while(op!=6);
}
