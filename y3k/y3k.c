#include<stdio.h>

int main(){

	unsigned long suma;
	int dia, mes , anio,
		diasMes[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
	freopen("input.in", "r", stdin);
	while(scanf("%lu %d %d %d", &suma, &dia, &mes, &anio) && suma != 0 && dia != 0 && mes != 0 && anio != 0){

		if(anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0))
					diasMes[2] = 29;

		while(dia + suma > diasMes[mes]){
			suma -= diasMes[mes];
				if(++mes > 12){
					mes = 1;
					++anio;
					if(anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0))
						diasMes[2] = 29;
					else
						diasMes[2] = 28;
				}
			}

		dia += suma;
		diasMes[2] = 28;
		printf("%d %d %d\n", dia, mes, anio);
	}
	return 0;
}
