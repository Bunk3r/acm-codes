#include<iostream>
#include<stack>
using namespace std;

int main(){
	short n, x;
	char flag, primero = 1;
	string cadena;
	stack<char> pila;
	char p;
	for(cin >> n ; n != 0 ; n--){
		getline(cin, cadena);
		if(primero){
			n++;
			primero = 0;
			continue;
		}
		if(cadena.empty()){
			cout << "Yes" << endl;
			continue;
		}
		else{
			x = 0;
			flag = 1;
			while(cadena[x]){
				switch(cadena[x]){
				case '(':
				case '[':
					pila.push(cadena[x]);
					break;
				case ')':
					if(pila.empty())
						flag = 0;
					else{
						p = pila.top();
						if(p == '(')
							pila.pop();
						else
							flag = 0;
					}
					break;
				case ']':
					if(pila.empty())
						flag = 0;
					else{
						p = pila.top();
						if(p == '[')
							pila.pop();
						else
							flag = 0;
					}
				}
				if(flag)
					x++;
				else
					break;
			}
			if(flag && pila.empty())
				cout << "Yes" << endl;
			else{
				cout << "No" << endl;
				while(!pila.empty())
					pila.pop();
			}
		}
	}
	return 0;
}
