#include<stdio.h>
#include<math.h>

char primos[100000] = {1,1};
int primosNumeros[100000];
int primosCantidad = 0;
int maximun = 0, maximunValue = 0;

void criba(){
	int x, y, max = sqrt(100000);
	for(x = 2 ; x <= max ; x++){
		if(!primos[x]){
			for(y = x ; x * y <= 100000 ; y++){
				primos[x*x] = 1;
			}
		}
	}
	for(x = 2 ; x < 100000 ; x++){
		if(!primos[x]){
			primosNumeros[primosCantidad++] = x;
		}
	}

}

int divisores(int numero){
	int x, iguales, divisores = 1;
	for(x = iguales = 0 ; x < primosCantidad && primosNumeros[x] * primosNumeros[x] <= numero  ; x++){
		while(numero%primosNumeros[x] == 0){
			numero /= primosNumeros[x];
			iguales++;
		}
		divisores *= iguales + 1;
		iguales = 0;
	}
	if(numero > 1){
		divisores <<= 1;
	}
	return divisores;
}

int main(){

	int N, L, U, caso, x, tmp;

	freopen("input.in", "r", stdin);

	scanf("%d", &N);

	criba();

	for(caso = 0 ; caso < N ; caso++){
		scanf("%d %d", &L, &U);

		 maximun = 0;
		 maximunValue = 0;

		if(L > U){
			L ^= U;
			U ^= L;
			L ^= U;
		}

		for(x = L ; x <= U ; x++){
			if((tmp = divisores(x)) > maximunValue){
				maximun = x;
				maximunValue = tmp;
			}
		}

		printf("Between %d and %d, %d has a maximum of %d divisors.\n", L, U, maximun, maximunValue);

	}

	return 0;
}
