#include<stdio.h>
#include<string.h>

int main(){
	int n , A , B , x;
	char player1[30], player2[30], flag;
	scanf("%d", &n);
	while(n--){
		scanf("%s", player1);
		scanf("%s", player2);
		A = strlen(player1);
		B = strlen(player2);
		if(A == B){
			flag = 1;
			for(x = 0; player1[x] && flag ; x++)
				switch(player1[x]){
				case 'a':
				case 'e':
				case 'i':
				case 'o':
				case 'u':
					switch(player2[x]){
						case 'a':
						case 'e':
						case 'i':
						case 'o':
						case 'u':
							break;
						default:
							flag = 0;
					}
					break;
				default:
					if(player1[x] != player2[x])
						flag = 0;
				}
			if(flag)
				printf("Yes\n");
			else
				printf("No\n");
		}
		else
			printf("No\n");
	}
	return 0;
}
