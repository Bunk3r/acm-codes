import java.io.*;
import java.util.Scanner;
import java.math.BigDecimal;

class Main
{
    public static void main (String args[]) 
    {
        Main myWork = new Main(); 
        myWork.Begin();           
    }

    void Begin()
    {
        int exponente;
        String valorEntrada;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Scanner entrada;
        try{
            do{
                valorEntrada = stdin.readLine();
                if(valorEntrada == null)
                    break;
                else{
                    entrada = new Scanner(valorEntrada);
                    entrada.useDelimiter("\\s+");
                    BigDecimal total = new BigDecimal("1");
                    BigDecimal valor = new BigDecimal(entrada.next());
                    exponente = Integer.parseInt(entrada.next());
                    total = valor.pow(exponente);
                    if(total.compareTo(BigDecimal.ONE) < 0){
                    	System.out.println(total.stripTrailingZeros().toPlainString().replace("0.", "."));
                    }
                    else{
                    	System.out.println(total.stripTrailingZeros().toPlainString());
                    }
                }
            }while(true);
        }
        catch (IOException e) {}
    }
}