import java.io.*;
import java.util.Scanner;
import java.math.BigInteger;

class Main
{
    public static void main (String args[]) 
    {
        Main myWork = new Main(); 
        myWork.Begin();           
    }

    void Begin()
    {
        BigInteger valor1, valor2;
        BigInteger maximo = new BigInteger("2147483647");
        String valores;
        String operador;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Scanner entrada;
        try{
            do{
                valores = stdin.readLine();
                if(valores == null)
                    break;
                else{
                	System.out.println(valores);
                    entrada = new Scanner(valores);
                    entrada.useDelimiter("\\s");
                    valor1 = new BigInteger(entrada.next());
                    operador = entrada.next();
                    valor2 = new BigInteger(entrada.next());
                    if(valor1.compareTo(maximo) == 1)
                        System.out.println("first number too big");
                    if(valor2.compareTo(maximo) == 1)
                        System.out.println("second number too big");
                    if(operador.equals("+"))
                        valor2 = valor2.add(valor1);
                    else
                        valor2 = valor2.multiply(valor1);
                    if(valor2.compareTo(maximo) == 1)
                        System.out.println("result too big");
                }
            }while(true);
        }
        catch (IOException e) {}
    }
}