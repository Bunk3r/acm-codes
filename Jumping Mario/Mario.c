#include<stdio.h>

int main(){

	int ciclos;
	int altos;
	int bajos;
	int paredes;
	int alturas[60];
	int caso = 1;
	int x;

		altos = 0;
		bajos = 0;

		scanf("%d", &ciclos);

		while(caso <= ciclos){
			altos = 0;
			bajos = 0;

			scanf("%d", &paredes);

			for(x = 0; x < paredes ; x++){
				scanf("%d", &alturas[x]);
				if(x > 0){
					if(alturas[x] > alturas[x - 1]){
						altos++;
					}else if(alturas[x] < alturas[x - 1]){
						bajos++;
					}
				}
			}

			printf("Case %d: %d %d\n", caso,altos,bajos);

			caso++;

		}

	return 0;
}
