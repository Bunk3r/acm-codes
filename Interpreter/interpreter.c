#include<stdio.h>
#include<stdlib.h>

int main(){

	char RAM[1000][5];
	int FLAG[10];
	int casos;
	int x;
	int caso = 0;
	int halt;
	int total;

	scanf("%d\n", &casos);

	while(casos != 0){
		total = 0;
		halt = 0;

		for(x = 0 ; x < 1000 ; x++){
			RAM[x][0] = '0';
			RAM[x][1] = '0';
			RAM[x][2] = '0';
		}

		for(x = 0 ; x < 10 ; x++)
			FLAG[x] = 0;

		x = 0;

		while(fgets(RAM[x], 5 , stdin) && RAM[x][0] != '\n')
			x++;

		x = 0;

		while(!halt){
			total++;
			switch(RAM[x][0]){
			case '0':
				if(FLAG[RAM[x][2] - 48] != 0)
					x = FLAG[RAM[x][1] - 48];
				else
					x++;
				break;
			case '2':
				FLAG[RAM[x][1] - 48] = RAM[x][2] - 48;
				x++;
				break;
			case '3':
				FLAG[RAM[x][1] - 48] += RAM[x][2] - 48;
				FLAG[RAM[x][1] - 48] %= 1000;
				x++;
				break;
			case '4':
				FLAG[RAM[x][1] - 48] = (FLAG[RAM[x][1] - 48] * (RAM[x][2] - 48))%1000;
				x++;
				break;
			case '5':
				FLAG[RAM[x][1] - 48] = FLAG[RAM[x][2] - 48];
				x++;
				break;
			case '6':
				FLAG[RAM[x][1] - 48] = (FLAG[RAM[x][1] - 48] + FLAG[RAM[x][2] - 48])%1000;
				x++;
				break;
			case '7':
				FLAG[RAM[x][1] - 48] = (FLAG[RAM[x][1] - 48] * FLAG[RAM[x][2] - 48])%1000;
				x++;
				break;
			case '8':
				FLAG[RAM[x][1] - 48] = atoi(RAM[FLAG[RAM[x][2] - 48]]);
				x++;
				break;
			case '9':
				sprintf(RAM[FLAG[RAM[x][2] - 48]],"%03d",FLAG[RAM[x][1] - 48]);
				x++;
				break;
			default:
				halt = 1;
			}
		}

		if(caso == 0)
					printf("%d\n", total);
		else
			printf("\n%d\n", total);
		casos--;
		caso++;
	}

	return 0;
}
