#include<stdio.h>
#include<string.h>

char field[105][105];
int oil;

void flood(int x, int y){
	if(x > 0 && y > 0 && field[x][y] != '\0')
		if(field[x][y] == '@'){
			field[x][y] = '*';
			flood(x - 1, y - 1);
			flood(x, y - 1);
			flood(x + 1, y - 1);
			flood(x - 1, y);
			flood(x + 1, y);
			flood(x - 1, y + 1);
			flood(x, y + 1);
			flood(x + 1, y + 1);
		}
}

void recorrer(int x, int y){
		field[x][y] = '*';
		oil++;
		flood(x - 1, y - 1);
		flood(x, y - 1);
		flood(x + 1, y - 1);
		flood(x - 1, y);
		flood(x + 1, y);
		flood(x - 1, y + 1);
		flood(x, y + 1);
		flood(x + 1, y + 1);
}

int main(){
	int x,y,m,n;
	char basura;

	while(scanf("%d %d\n", &m, &n) && m != 0){
		memset(field, 0, sizeof(field));
		for(x = 1 ; x <= m ; x++, scanf("%c", &basura))
			for(y = 1 ; y <= n ; y++)
				scanf("%c",&field[x][y]);
		oil = 0;
		for(x = 1 ; x <= m ; x++)
			for(y = 1 ; y <= n ; y++)
				if(field[x][y] == '@')
					recorrer(x, y);
		printf("%d\n", oil);
	}
	return 0;
}
