#include<stdio.h>

int main(){

	int T, N, POS, min, max;
	freopen("input.in", "r", stdin);
	scanf("%d", &T);
	while(T--){
		scanf("%d", &N);
		min = 100;
		max = 0;
		while(N--){
			scanf("%d", &POS);
			if(POS < min)
				min = POS;
			if(POS > max)
				max = POS;
		}
		printf("%d\n", (max - min)<<1);
	}
	return 0;
}
