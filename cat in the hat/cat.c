#include<stdio.h>
#include<math.h>
int main(){
  int a,g,k,n,m,sum;
  while(scanf("%d %d",&a,&g)&&a!=0){
    if(g != 1 && a != 1){
		for(k=2;k<=g;k++)
			if(g%k==0&&a%k==1&&(g-1)%(k-1)==0&&a%(k+1)==0){
				n=g;
				m=a;
				while(n%k==0&&m%(k+1)==0){
					n=n/k;
					m=m/(k+1);
				}
				if(n==1)break;
			}
		printf("%d %d\n",(g-1)/(k-1),a*(k+1)-g*k);
    }
    else{
    	n = 0;
    	m = a;
    	while(a > 1){
    		a >>= 1;
    		n++;
    	}
    	printf("%d ", n);
    	sum = 0;
    	while(m){
    		sum += m;
    		m >>= 1;
    	}
    	printf("%d\n", sum);
    }
  }
  return 0;
}
