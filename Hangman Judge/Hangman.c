#include<stdio.h>

int main(){

	int ronda;
	char palabra[10000];
	char intento[10000];
	int errores;
	char letras[100];
	int x;
	int y;
	int correcto;
	int repetido;
	int termino;

	do{

		scanf("%d\n", &ronda);

		if(ronda == -1)
			break;

		x = 0;
		errores = 0;

		scanf("%s%s", &palabra[0], &intento[0]);

		while(x < 100){
			letras[x] = '*';
			x++;
		}


		x = 0;
		while(intento[x]){
			y = 0;
			correcto = 0;
			repetido = 0;

			if(intento[x] == '\n')
				break;

			while(y < x){
				if(intento[x] == intento[y]){
					repetido = 1;
					break;
				}
			y++;
			}

			if(!repetido){

				y = 0;

				while(palabra[y]){

					if(palabra[y] == intento[x]){
						palabra[y] = 'X';
						correcto = 1;
					}
					y++;

				}

				if(!correcto){
					y = 0;
					repetido = 0;

					while(letras[y] != '*'){
						if(letras[y] == intento[x])
							repetido = 1;
					y++;
					}

					if(!repetido){
						errores++;
						letras[y] = intento[x];
					}
				}else{
					y = 0;
					while(letras[y] != '*')
						y++;
					letras[y] = intento[x];
				}

				if(errores > 6){
					printf("Round %d\nYou lose.\n", ronda);
					break;
				}

				y = 0;
				termino = 1;

				while(palabra[y]){

					if(palabra[y] != 'X' && palabra[y] != '\n'){
						termino = 0;
						break;
					}

					y++;
				}

				if(termino){
					printf("Round %d\nYou win.\n", ronda);
					break;
				}

		}
				x++;
		}

		if(errores != 7 && termino != 1)
			printf("Round %d\nYou chickened out.\n", ronda);


	}while(1);

	return 0;
}
