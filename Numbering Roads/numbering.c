#include<stdio.h>

#define MAX 27

int main(){
	int R, N, x, caso = 1;
	freopen("input.in", "r", stdin);
	while(scanf("%d %d", &R, &N) && R != 0)
		for(x = 1; x <= MAX ; x++)
			if(x*N >= R){
				printf("Case %d: %d\n", caso++, x - 1);
				break;
			}
			else if(x == MAX)
				printf("Case %d: impossible\n", caso++);
	return 0;
}
