#include<stdio.h>
#include<string.h>

char Canvas[251][251], nuevo, actual;
int M = 0 , N = 0;

int main(){
	char opcion , nombre[13] , color;
	int x1 , x2 , y1 , y2 , aux;
	void Fill(int X, int Y);


	while(scanf("%c", &opcion) != EOF){

		if(opcion == 'X')
			return 0;

		switch(opcion){

			case 'I':
				scanf("%d %d", &M, &N);
				memset(Canvas, sizeof(Canvas), '\0');

			case 'C':
				for(y1 = 1; y1 <= N ; y1++)
					for(x1 = 1; x1 <= M ; x1++)
						Canvas[y1][x1] = 'O';
				break;

			case 'L':
				scanf("%d %d %c", &x1, &y1, &color);
				Canvas[y1][x1] = color;
				break;

			case 'V':
				scanf("%d %d %d %c", &x1, &y1, &y2, &color);
				if(y1 > y2)
					for(; y2 <= y1; y2++)
						Canvas[y2][x1] = color;
				else
					for(; y1 <= y2; y1++)
						Canvas[y1][x1] = color;
				break;

			case 'H':
				scanf("%d %d %d %c", &x1, &x2, &y1, &color);
				if(x1 > x2)
					for(; x2 <= x1; x2++)
						Canvas[y1][x2] = color;
				else
					for(; x1 <= x2; x1++)
						Canvas[y1][x1] = color;
				break;

			case 'K':
				scanf("%d %d %d %d %c", &x1, &y1, &x2, &y2, &color);
				if(x1 > x2){
					aux = x1;
					x1 = x2;
					x2 = aux;
				}
				if(y1 > y2){
					aux = y1;
					y1 = y2;
					y2 = aux;
				}
				for(; y1 <= y2; y1++)
					for(aux  = x1; aux <= x2; aux++)
						Canvas[y1][aux] = color;
				break;

			case 'F':
				scanf("%d %d %c", &x1, &y1, &color);
				if(x1 > 0 && x1 < M && y1 > 0 && y1 < N)
					if(Canvas[y1][x1] != color){
						nuevo = color;
						actual = Canvas[y1][x1];
						Fill(x1 , y1);
					}
				break;

			case 'S':
				scanf("%s", &nombre[0]);
					printf("%s\n", nombre);
				for(y1 = 1; y1 <= N ; y1++){
					for(x1 = 1; x1 <= M ; x1++)
						printf("%c", Canvas[y1][x1]);
						printf("\n");
				}
				break;

			default:
				break;
		}

	}
	return 0;
}

void Fill(int X, int Y){
		Canvas[Y][X] = nuevo;
		if(Y + 1 <= N && Canvas[Y + 1][X] == actual)
			Fill(X, Y + 1);
		if(Y - 1 > 0 && Canvas[Y - 1][X] == actual)
			Fill(X, Y - 1);
		if(X + 1 <= M && Canvas[Y][X + 1] == actual)
			Fill(X + 1, Y);
		if(X - 1 > 0 && Canvas[Y][X - 1] == actual)
			Fill(X - 1, Y);
}
