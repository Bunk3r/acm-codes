#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>

#define MILI 0.001
#define KILO 1000
#define MEGA 1000000

int main(){
    int flag1 , flag2, flag3, limite, cont = 1, i, a, x; /* Declaren todos los del mismo tipo en una linea */
    char cadena[200], Valor[20];
	float voltaje, corriente, potencia;

			freopen("input.in", "r", stdin);

			scanf("%d\n", &limite);
			while(limite--)
			{
				fgets(cadena,200,stdin);
				flag1 = flag2 = flag3 = voltaje = corriente = potencia = 0;
				for( i = 0 ; cadena[i] ; i++ )
				{
					memset(Valor , 0 , sizeof(Valor));
					if( cadena[i] == 'U' && cadena[i+1] == '=')
					{
						a = i + 2;
						x = 0;
						flag1 = 1;
						while(isdigit(cadena[a]) != 0 || cadena[a] == '.' || cadena[a] == '-')
							Valor[x++] = cadena[a++];
						voltaje = atof(Valor);
						switch(cadena[a]){
						case 'm':
							voltaje = voltaje * MILI;
							break;
						case 'M':
							voltaje = voltaje * MEGA;
							break;
						case 'k':
							voltaje = voltaje * KILO;
							break;
						}
					}
					else if( cadena[i] == 'I' && cadena[i+1] == '=')
					{
						a = i+2;
						x=0;
						flag2 = 1;
						while( isdigit(cadena[a]) != 0 || cadena[a] == '.' || cadena[a] == '-')
							Valor[x++] = cadena[a++];
						corriente = atof(Valor);
						switch(cadena[a]){
						case 'm':
							corriente = corriente * MILI;
							break;
						case 'M':
							corriente = corriente * MEGA;
							break;
						case 'k':
						    corriente = corriente *KILO;
						    break;
						}
                  }
                  else if( cadena[i] == 'P' && cadena[i+1] == '=')
                  {
						a=i+2;
						x=0;
						flag3 = 1;
						while(isdigit(cadena[a]) != 0 || cadena[a] == '.' || cadena[a] == '-')
							Valor[x++] = cadena[a++];
						potencia = atof(Valor);
						switch(cadena[a]){
						case 'm':
							potencia = potencia * MILI;
							break;
						case 'M':
							potencia = potencia * MEGA;
							break;
						case 'k':
							potencia = potencia * KILO;
							break;
						}
					}
                }

					if(flag3 == 0)
					{
						potencia = corriente * voltaje;
						printf("Problem #%d\n",cont++);
						printf("P=%.2fW\n",potencia);

					}
					else if(flag2 == 0)
					{
						corriente = potencia/voltaje;
						printf("Problem #%d\n", cont++);
						printf("I=%.2fA\n",corriente);

					}
					else if( flag1 == 0)
					{
						voltaje = potencia/corriente;
						printf("Problem #%d\n", cont++);
						printf("U=%.2fV\n",voltaje);
					}
			}
return 0;
}
