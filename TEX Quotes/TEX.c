#include<stdio.h>

int main(){
	char c;
	int left = 1;
	while(scanf("%c", &c) != EOF)
		switch(c){
		case '"':
			if(left){
				printf("``");
				left = 0;
			}
			else{
				printf("''");
				left = 1;
			}
			break;
		default:
			printf("%c", c);
		}
	return 0;
}
