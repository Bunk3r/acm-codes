#include<stdio.h>

int main(){

	char Tablero[12][12];
	int Game = 1;
	int x;
	int y;
	int Wx;
	int Wy;
	int Bx;
	int By;
	int HackeW;
	int HackeB;

	freopen("input.in", "r", stdin);

	for(y = 0 ; y < 12 ; y++)
		for(x = 0 ; x < 12 ; x ++)
			Tablero[y][x] = '\0';

	while(1){

		Wx =  89;
		HackeW = 0;
		HackeB = 0;

		for(y = 2 ; y < 10 ; y++){
			for(x = 2 ; x < 10 ; x ++){
				scanf("%c", &Tablero[y][x]);
					if(Tablero[y][x] == 'K'){
						Wx = x;
						Wy = y;
					}else if(Tablero[y][x] == 'k'){
						Bx = x;
						By = y;
					}
			}
			scanf("\n");
		}
		scanf("\n");

		if(Wx != 89){

			/* White Pawn & Black Pawn */
			if(Tablero[Wy - 1][Wx + 1] == 'p' || Tablero[Wy - 1][Wx - 1] == 'p')
				HackeW = 1;
			else if(Tablero[By + 1][Bx - 1] == 'P' || Tablero[By + 1][Bx + 1] == 'P')
				HackeB = 1;

			/* Black Rook & Black Queen vertically or horizontally */
			y = Wy + 1;
			x = Wx;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'r' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				y++;
			}
			y = Wy - 1;
			x = Wx;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'r' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				y--;
			}
			y = Wy;
			x = Wx + 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'r' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				x++;
			}
			y = Wy;
			x = Wx - 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'r' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				x--;
			}

			/* White Rock & White Queen vertically or horizontally */
			y = By + 1;
			x = Bx;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'R' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				y++;
			}
			y = By - 1;
			x = Bx;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'R' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				y--;
			}
			y = By;
			x = Bx + 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'R' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				x++;
			}
			y = By;
			x = Bx - 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'R' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				x--;
			}

			/* White Knight & Black Knight */
			y = By;
			x = Bx;
			if(Tablero[y - 1][x - 2] == 'N' || Tablero[y - 1][x + 2] == 'N' || Tablero[y - 2][x - 1] == 'N' || Tablero[y - 2][x + 1] == 'N' || Tablero[y + 1][x - 2] == 'N' || Tablero[y + 1][x + 2] == 'N' || Tablero[y + 2][x - 1] == 'N' || Tablero[y + 2][x + 1] == 'N')
				HackeB = 1;

			y = Wy;
			x = Wx;
			if(Tablero[y - 1][x - 2] == 'n' || Tablero[y - 1][x + 2] == 'n' || Tablero[y - 2][x - 1] == 'n' || Tablero[y - 2][x + 1] == 'n' || Tablero[y + 1][x - 2] == 'n' || Tablero[y + 1][x + 2] == 'n' || Tablero[y + 2][x - 1] == 'n' || Tablero[y + 2][x + 1] == 'n')
				HackeW = 1;

			/* Black Bishop & Black Queen diagonally */
			y = Wy - 1;
			x = Wx + 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'b' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				y--;
				x++;
			}
			y = Wy - 1;
			x = Wx - 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'b' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				y--;
				x--;
			}
			y = Wy + 1;
			x = Wx + 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'b' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				x++;
				y++;
			}
			y = Wy + 1;
			x = Wx - 1;
			while(Tablero[y][x] && HackeW == 0){
				if(!(Tablero[y][x] == 'b' || Tablero[y][x] == 'q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeW = 1;
				x--;
				y++;
			}

			/* White Bishop & White Queen diagonally */
			y = By + 1;
			x = Bx - 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'B' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				y++;
				x--;
			}
			y = By + 1;
			x = Bx + 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'B' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				y++;
				x++;
			}
			y = By - 1;
			x = Bx + 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'B' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				x++;
				y--;
			}
			y = By - 1;
			x = Bx - 1;
			while(Tablero[y][x] && HackeW == 0 && HackeB == 0){
				if(!(Tablero[y][x] == 'B' || Tablero[y][x] == 'Q' || Tablero[y][x] == '.'))
					break;
				else if(Tablero[y][x] != '.')
					HackeB = 1;
				x--;
				y--;
			}

			if(HackeW)
				printf("Game #%d: white king is in check.\n", Game);
			else if(HackeB)
				printf("Game #%d: black king is in check.\n", Game);
			else
				printf("Game #%d: no king is in check.\n", Game);

		}
		else{
			break;
		}

		Game++;

	}

	return 0;
}
