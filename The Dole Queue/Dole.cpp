#include<cstdio>
#include<string>
using namespace std;

int main(){

	unsigned int M, K, m, k, x,y, N;
	char coma, lista[20], F;

	while(scanf("%d", &N) && N != 0){
		scanf("%d %d", &K, &M);
		coma = F = x = 1;
		y = N;
		memset(lista, 1, N + 1);
		lista[0] = 0;
		do{
			k = 0;
			do{
				if(lista[x] == 1)
					if(++k == K)
						break;
				if(x == N)
					x = 1;
				else
					x++;
			}while(1);
			k = x;
			m = 0;
			do{
				if(lista[y] == 1)
					if(++m == M)
						break;
				if(y == 1)
					y = N;
				else
					y--;
			}while(1);
			m = y;
			if(coma)
				if(k == m){
					if(k < 10)
						printf("  %d", k);
					else
						printf(" %d", k);
					lista[k] = 2;
					coma--;
					F++;
				}
				else{
					if(k < 10 && m < 10)
						printf("  %d  %d", k, m);
					else if(k < 10)
						printf("  %d %d", k, m);
					else if(m < 10)
						printf(" %d  %d", k, m);
					else
						printf("  %d  %d", k, m);
					lista[k] = lista[m] = 2;
					coma--;
					F++;

				}
			else
				if(k == m){
					if(k < 10)
						printf(",  %d", k);
					else
						printf(", %d", k);
					lista[k] = 2;
					F++;
				}
				else{
					if(k < 10 && m < 10)
						printf(",  %d  %d", k, m);
					else if(k < 10)
						printf(",  %d %d", k, m);
					else if(m < 10)
						printf(", %d  %d", k, m);
					else
						printf(",  %d  %d", k, m);
					lista[k] = lista[m] = 2;
					F += 2;
				}
		}while(F < N);
		printf("\n");
	}

	return 0;
}
