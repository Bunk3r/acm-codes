#include<stdio.h>

int alive, N, totalAlive;
char ocean[102][102];

void cleanRest(int y, int x){

	if(y < 1 || x < 1 || y > N || x > N)return;

	if(ocean[y][x] == 'x')alive++;

	if(ocean[y][x] != '.'){
		ocean[y][x] = '.';
		cleanRest(y - 1, x);
		cleanRest(y, x - 1);
		cleanRest(y + 1, x);
		cleanRest(y, x + 1);
	}

}

int main(){

	int Cases, Case, x, y;

	freopen("input.in", "r", stdin);

	scanf("%d", &Cases);

	for(Case = 1 ; Case <= Cases ; Case++){

		scanf("%d\n", &N);
		totalAlive = 0;

		for(y = 1 ; y <= N ; y++){
			for(x = 1 ; x <= N ; x++){
				scanf("%c", &ocean[y][x]);
			}
			scanf("\n");
		}

		for(y = 1 ; y <= N ; y++){
			for(x = 1 ; x <= N ; x++){
				if(ocean[y][x] != '.'){
					alive = 0;
					cleanRest(y, x);
					if(alive)totalAlive++;
				}
			}
		}

		printf("Case %d: %d\n",Case, totalAlive);

	}

	return 0;
}
