#include<stdio.h>
int main(){
	int casos,N,M;
	short div[10000];
	scanf("%d", &casos);
	do{
		scanf("%d %d", &M , &N);
		div[M] = div[M] == 0 ? M/3 : div[M];
		div[N] = div[N] == 0 ? N/3 : div[N];
		printf("%d\n",div[M] * div[N]);
	}while(--casos);
	return 0;
}
