#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
#define N 100000


vector<int> Edge[N];
bool visited[N];
int inedge[N];


bool dfs(int now){
  visited[now] = true;
  for(unsigned int i = 0 ; i < Edge[now].size() ; i++){
    int next = Edge[now][i];
    if ( visited[next] == true)
    	return false;
    if ( dfs(next) == false)
    	return false;
  }
  return true;
}

bool solve(int n){
  int cnt = 0;
  for(int i = 0 ; i < n ; i++){
    if ( visited[i] == true || inedge[i] == 1)
    	continue;
    if ( inedge[i] > 1)
    	return false;
    if (cnt++ == 1)
    	return false;
    if (dfs(i) == false)
    	return false;
  }
  for(int i=0;i<N;i++)
    if ( visited[i] == false)return false;
  return true;
}

int main(){
  int tc = 1 , f , t;
  while(scanf("%d %d", &f, &t) && f != -1){

    fill(visited,visited+N,true);
    for(int i = 0 ; i < N ; i++)
    	Edge[i].clear();
    fill(inedge,inedge+N,0);

    while(true){
      if (f == 0 && t == 0)
    	  break;
      Edge[f].push_back(t);
      inedge[t]++;
      visited[f] = visited[t] = false;
      scanf("%d %d", &f, &t);
    }

    if ( solve(N))
    	printf("Case %d is a tree.\n",tc++);
    else
    	printf("Case %d is not a tree.\n",tc++);
  }
  return 0;
}
