#include <iostream>
#include <list>
using namespace std;

list<char> lista;
list<char>::iterator p;

int main()
{

	freopen("input.in", "r", stdin);
	string entrada;

	while(getline(cin,entrada) && !cin.eof()){
		int i = 0;
		while(entrada[i] != '?')
			lista.push_front(entrada[i++]);

		while(!lista.empty()){
			p = lista.begin();
			cout << *p;
			lista.pop_front();
		}
		cout << endl;
	}

  return 0;
}
