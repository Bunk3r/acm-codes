#include<iostream>
#include<cstdio>
#include<map>
#include <string>

using namespace std;

int pos;
char tmp[3000];

int main(){

	int T, caso = 1, x, isSpace;
	string codigo;
	char  letter[10];
	map<string, char> morse;

	freopen("input.in", "r", stdin);

	morse.insert(pair<string, char>(".-",'A'));
	morse.insert(pair<string, char>("-...",'B'));
	morse.insert(pair<string, char>("-.-.",'C'));
	morse.insert(pair<string, char>("-..",'D'));
	morse.insert(pair<string, char>(".",'E'));
	morse.insert(pair<string, char>("..-.",'F'));
	morse.insert(pair<string, char>("--.",'G'));
	morse.insert(pair<string, char>("....",'H'));
	morse.insert(pair<string, char>("..",'I'));
	morse.insert(pair<string, char>(".---",'J'));
	morse.insert(pair<string, char>("-.-",'K'));
	morse.insert(pair<string, char>(".-..",'L'));
	morse.insert(pair<string, char>("--",'M'));
	morse.insert(pair<string, char>("-.",'N'));
	morse.insert(pair<string, char>("---",'O'));
	morse.insert(pair<string, char>(".--.",'P'));
	morse.insert(pair<string, char>("--.-",'Q'));
	morse.insert(pair<string, char>(".-.",'R'));
	morse.insert(pair<string, char>("...",'S'));
	morse.insert(pair<string, char>("-",'T'));
	morse.insert(pair<string, char>("..-",'U'));
	morse.insert(pair<string, char>("...-",'V'));
	morse.insert(pair<string, char>(".--",'W'));
	morse.insert(pair<string, char>("-..-",'X'));
	morse.insert(pair<string, char>("-.--",'Y'));
	morse.insert(pair<string, char>("--..",'Z'));
	morse.insert(pair<string, char>("-----",'0'));
	morse.insert(pair<string, char>(".----",'1'));
	morse.insert(pair<string, char>("..---",'2'));
	morse.insert(pair<string, char>("...--",'3'));
	morse.insert(pair<string, char>("....-",'4'));
	morse.insert(pair<string, char>(".....",'5'));
	morse.insert(pair<string, char>("-....",'6'));
	morse.insert(pair<string, char>("--...",'7'));
	morse.insert(pair<string, char>("---..",'8'));
	morse.insert(pair<string, char>("----.",'9'));
	morse.insert(pair<string, char>(".-.-.-",'.'));
	morse.insert(pair<string, char>("--..--",','));
	morse.insert(pair<string, char>("..--..",'?'));
	morse.insert(pair<string, char>(".----.",'\''));
	morse.insert(pair<string, char>("-.-.--",'!'));
	morse.insert(pair<string, char>("-..-.",'/'));
	morse.insert(pair<string, char>("-.--.",'('));
	morse.insert(pair<string, char>("-.--.-",')'));
	morse.insert(pair<string, char>(".-...",'&'));
	morse.insert(pair<string, char>("---...",':'));
	morse.insert(pair<string, char>("-.-.-.",';'));
	morse.insert(pair<string, char>("-...-",'='));
	morse.insert(pair<string, char>(".-.-.",'+'));
	morse.insert(pair<string, char>("-....-",'-'));
	morse.insert(pair<string, char>("..--.-",'_'));
	morse.insert(pair<string, char>(".-..-.",'"'));
	morse.insert(pair<string, char>(".--.-.",'@'));

	scanf("%d\n", &T);

	for( ; caso <= T ; caso++){
		if(caso > 1)
			cout << endl;
		cout << "Message #" << caso << endl;
		fgets(tmp, 3000, stdin);
		for(pos = x = isSpace = 0 ; ; pos++){
			if((tmp[pos] == ' ' || tmp[pos] == '\n' || tmp[pos] == '\0') && x == 0 && pos != 0 && isSpace == 0){
				cout << " ";
				isSpace = 1;
			}
			else if(tmp[pos] == ' ' || tmp[pos] == '\n' || tmp[pos] == '\0' && pos != 0){
				letter[x] = '\0';
				codigo.assign(letter);
				cout << morse[codigo];
				x = 0;
				isSpace = 0;
			}
			else{
				letter[x++] = tmp[pos];
				isSpace = 0;
			}
			if(tmp[pos] == '\n' || tmp[pos] == '\0')break;
		}
		cout << endl;
	}

	return 0;
}
