#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<windows.h>

#define cpy(A,B)	strcpy(Enfermedades[A].pregunta,B);
#define ad(A,B)		Enfermedades[A].ad[Enfermedades[A].adyacentes++] = B;
#define print(A)	printf("\t%s\n\n", Enfermedades[A].pregunta);
#define Linea		printf("--------------------------------------------------------------------------------\n");

/* Se declaran las funciones a utilizar */
void imprime_menu(), imprime_mensaje(char msg[]), limpiar_panntalla(), nuevo_paciente(), obtener_info(), imprime_receta(), crear_arbol(), menu_arbol(), recorrer_arbol(int actual);

typedef struct{
	int ad[4];
	char pregunta[120];
	int adyacentes;
}nodo;

nodo Enfermedades[23];

/* Variables utilizadas para guardar la info del paciente */
char nombre[50], edad[4], estatura[4], peso[4], no_seguro[12], arbol[4];

void crear_arbol(){
	memset(Enfermedades, 0, sizeof(Enfermedades));
	cpy(0,"Qu� tipo de artritis padece?")
	ad(0,1)
	ad(0,2)
	cpy(1,"OSTEOARTRITIS")
	ad(1,20)
	cpy(2,"REUMATOIDE")
	ad(2,19)
	cpy(20,"Inyectar liquido sinovial artificial, Cortisona.")
	cpy(19,"Imuran, Rheumatrex, Cytoxan, Anakinra.")
	cpy(3,"Padece colesteron alto?")
	ad(3,12)
	cpy(12,"ALTO")
	ad(12,13)
	cpy(13,"Dilucid  y Vitamina B. Dieta y minimo 30 minutos de ejercicio.")
	cpy(4,"Que tipo de diabetes padece?")
	ad(4,5)
	cpy(5,"PREDIABETES")
	ad(5,17)
	cpy(17,"Solo cuidados y examenes de rutina por consulta realizada.")
	ad(4,6)
	cpy(6,"TIPO1")
	ad(6,16)
	cpy(16,"Inyeccion de insulina diariamente de 5 mlg.")
	ad(4,7)
	cpy(7,"TIPO2")
	ad(7,15)
	cpy(15,"Metformin, glimepirida o insulina con una dosis de dos insulinas por dia.")
	ad(4,8)
	cpy(8,"PRENATAL")
	ad(8,14)
	cpy(14,"Mantener los niveles de az�car durante el embarazo y vigilar el bebe asi como realizar chequeos con ginecologo.")
	cpy(9,"Que tipo de presion padece?");
	ad(9,10)
	cpy(10,"ALTA")
	ad(10,21)
	cpy(21,"Dafnest, Ensibest.")
	ad(9,11)
	cpy(11,"BAJA")
	ad(11,22)
	cpy(22,"Implementacion de un marcapasos.")
}

void imprime_mensaje(char msg[]){
	int x = (40 - strlen(msg))>>1;
	printf("\n\n\n\n\n\n\n\n\n");
	printf("\t\t****************************************\n");
	printf("\t\t*                                      *\n");
	printf("\t\t*                                      *\n");
	printf("\t\t");
	for(;x!=0;x--)printf(" ");
	printf("%s\n", msg);
	printf("\t\t*                                      *\n");
	printf("\t\t*                                      *\n");
	printf("\t\t****************************************\n");
	printf("\n\n\n\n\n\n\n\n");
	Sleep(4000);
}

void limpiar_pantalla(){
	printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
}

void nuevo_paciente(){
	char caracter;
	int z = 0;
	FILE *pacientes;
	pacientes = fopen("pacientes.info","a");
	limpiar_pantalla();
	printf("\n\n\n\n\tIngresar el No. de Seguro Social del paciente...\n\t");
	while((caracter = getchar()) != '\n'){
		no_seguro[z++] = caracter;
	 	fputc(caracter, pacientes);
	}
	fputc(',', pacientes);
	z = 0;
	printf("\n\n\n\n\tIngresar el nombre del paciente...\n\t");
	while((caracter = getchar()) != '\n'){
		caracter = toupper(caracter);
		nombre[z++] = caracter;
	 	fputc(caracter, pacientes);
	}
	fputc(',', pacientes);
	z = 0;
	printf("\n\n\n\n\tIngresar la edad del paciente...\n\t");
	while((caracter = getchar()) != '\n'){
		edad[z++] = caracter;
	 	fputc(caracter, pacientes);
	}
	 fputc(',', pacientes);
	 z = 0;
	printf("\n\n\n\n\tIngresar la estatura del paciente(estatura en cm)...\n\t");
	while((caracter = getchar()) != '\n'){
		estatura[z++] = caracter;
		fputc(caracter, pacientes);
	}
	fputc(',', pacientes);
	z = 0;
	printf("\n\n\n\n\tIngresar el peso del paciente(peso en kg)...\n\t");
	while((caracter = getchar()) != '\n'){
		peso[z++] = caracter;
	 	fputc(caracter, pacientes);
	}
	fputc(',', pacientes);
	menu_arbol();
	fprintf(pacientes,"%s\n", arbol);
	imprime_receta();
	fclose(pacientes);
	getchar();
	imprime_mensaje("Paciente Agregado Exitosamente");
	imprime_menu();
}

void obtener_info(){
	char datos[100][200], tmp[11], caracter;
	int x = 0, y, z;
	FILE *conocido;
	conocido = fopen("pacientes.info","r");
	printf("\n\n\n\tIngrese el No. de seguro social a buscar.\n\t");
	while((caracter = getchar()) != '\n')
		no_seguro[x++] = caracter;
	x = 0;
	while(fgets(datos[x], 200, conocido) != NULL){
		strncpy(tmp, datos[x], 10);
		if(strcmp(no_seguro, tmp) == 0){
			y = 11;
			z = 0;
			while(datos[x][y] != ',')nombre[z++] = datos[x][y++];
			z = 0;
			y++;
			while(datos[x][y] != ',')edad[z++] = datos[x][y++];
			z = 0;
			y++;
			while(datos[x][y] != ',')estatura[z++] = datos[x][y++];
			z = 0;
			y++;
			while(datos[x][y] != ',')peso[z++] = datos[x][y++];
			z = 0;
			y++;
			while(datos[x][y] != '\n')arbol[z++] = datos[x][y++];
			imprime_receta();
			break;
		}
		else
			x++;
	}
	fclose(conocido);
	imprime_menu();
}

void imprime_menu(){
	int op;
	printf("\n\n\n\n\n\n\n\t\t\tMen�:\n\n\n\n");
	printf("\t1.- Ingresar un paciente nuevo.\n\n");
	printf("\t2.- Tratar paciente conocido.\n\n");
	printf("\t3.- Salir.\n\n\n\n\n\n\n\n\t->");
	scanf("%d", &op);
	getchar();
	switch(op){
		case 1:
			limpiar_pantalla();
			nuevo_paciente();
			break;
		case 2:
			limpiar_pantalla();
			obtener_info();
			break;
		case 3:
			break;
		default:
			imprime_mensaje("Ingrese alguna de las opciones");
			imprime_menu();
		}
}

void menu_arbol(){
	limpiar_pantalla();
	int op;
	printf("\n\n\n\n\n\n\n\t\tQue enfermedad padece ?:\n\n\n");
	printf("\t1.- Artritis.\n\n");
	printf("\t2.- Diabetes.\n\n");
	printf("\t3.- Colesterol.\n\n");
	printf("\t4.- Presion.\n\n\n\n\n\n\n\n\t->");
	scanf("%d", &op);
	switch(op){
	case 1:
		recorrer_arbol(0);
		break;
	case 2:
		recorrer_arbol(4);
		break;
	case 3:
		recorrer_arbol(3);
		break;
	case 4:
		recorrer_arbol(9);
		break;
	default:
		imprime_mensaje("Ingrese alguna de las opciones");
		menu_arbol();
	}
}

void recorrer_arbol(int actual){
 	int x = 0;
	limpiar_pantalla();
	printf("\n\n\n");
	Linea
	print(actual)
	for(;x < Enfermedades[actual].adyacentes;x++)
		printf("\t%d.- %s\n\n", x + 1, Enfermedades[Enfermedades[actual].ad[x]].pregunta);
	printf("\n\tElija una opcion ->");
	scanf("%d", &x);
	x--;
	itoa(Enfermedades[Enfermedades[actual].ad[x]].ad[0], arbol,10);
}

void imprime_receta(){
	limpiar_pantalla();
	int receta = atoi(arbol);
	Linea
	printf("\tDoctor: Dr. House\n\n");
	printf("\tClinica: Hospital Regional No. 37\n\n");
	Linea
	printf("\tPaciente: %s\n\n", nombre);
	printf("\tEdad: %s\n\n", edad);
	printf("\tEstatura: %s cm.\t\tPeso: %s kg.\n\n", estatura, peso);
	Linea
	printf("\tReceta Medica:\n\n");
	printf("\t%s\n\n", Enfermedades[receta].pregunta);
	printf("--------------------------------------------------------------------------------\nPresionar enter para continuar...");
	memset(nombre,0,sizeof(nombre));
	memset(no_seguro,0,sizeof(no_seguro));
	memset(edad,0,sizeof(edad));
	memset(estatura,0,sizeof(estatura));
	memset(peso,0,sizeof(peso));
	memset(arbol,0,sizeof(arbol));
	getchar();
}

int main(){

	imprime_mensaje("Bienvenido");
	crear_arbol();
	imprime_menu();

	return 0;
}
