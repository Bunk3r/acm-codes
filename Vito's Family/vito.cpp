#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;

vector<int> calles;
vector<int>::iterator p;

int main(){
	int N, R, total, pos,med, x;
	freopen("input.in", "r", stdin);
	scanf("%d", &N);
	while(N--){
		total = 0;
		scanf("%d", &R);
		while(R--){
			scanf("%d", &x);
			calles.push_back(x);
		}
		sort(calles.begin(),calles.end());
		pos = calles.size();
		if(pos&1)
			pos>>=1;
		else{
			pos--;
			pos>>=1;
		}
		med = calles[pos];
		p = calles.begin();
		while(p != calles.end())
			total += *p > med ? *p++ - med : med - *p++;
		printf("%d\n", total);
		calles.clear();
	}
	return 0;
}
