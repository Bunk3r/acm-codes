#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;

typedef struct{
	int id;
	int distancia;
}nodo;

vector<nodo> calles[105];
vector<nodo>::iterator sig;

int cmpnodo(const nodo& A, const nodo& B){
	return (A.distancia - B.distancia) * -1;
}

int llego, N, M;
unsigned long long Primero, Segundo;

void recorrer(int actual, unsigned long long Distancia, int X){
		if(actual == N){
			llego = 1;
			if(Primero != 0){
				if(Distancia < Primero){
						Segundo = Primero;
						Primero = Distancia;
				}
				else if((Segundo != 0 && Distancia < Segundo) || Segundo == 0)
						Segundo = Distancia;
			}
			else{
				Primero = Distancia;
			}
			llego++;
			recorrer(1, 0, llego);
		}
		else
		while(calles[actual].begin() != calles[actual].end() && llego == X){
			nodo tmp;
			sort(calles[actual].begin(), calles[actual].end(), cmpnodo);
			tmp = *(calles[actual].begin());
			sig = calles[tmp.id].begin();
			while(sig != calles[tmp.id].end())
				if((*sig).id == actual)
					calles[tmp.id].erase(sig);
				else
					sig++;
			calles[actual].erase(calles[actual].begin());
			recorrer(tmp.id, Distancia + tmp.distancia, X);
		}
}


int main(){
	int x, nodoI, nodoF;
	nodo tmp;
	freopen("input.in", "r", stdin);
	while(scanf("%d", &N) && N != 0){
		for(x = 1 ; x <= N ; x++)
			calles[x].clear();
		scanf("%d", &M);
		for(x = 0 ; x < M ; x++){
			scanf("%d %d %d", &nodoI, &nodoF, &tmp.distancia);
			tmp.id = nodoF;
			calles[nodoI].insert(calles[nodoI].begin(), tmp);
			tmp.id = nodoI;
			calles[nodoF].insert(calles[nodoF].begin(), tmp);
		}

		Primero = Segundo = llego = 0;
		recorrer(1, 0, 0);
		if(Primero && Segundo)
			printf("%llu\n", Primero + Segundo);
		else
			printf("Back to jail\n");
	}

	return 0;
}
