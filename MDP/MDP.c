#include<stdio.h>
#include<string.h>

double matrix[2][4] = {{0,0,0,0},{-100,0,0,100}};
double newMatrix[2][4] = {{0,0,0,0},{-100,0,0,100}};
char moveMatrix[2][4] = {};

int validateX(int x){
	if(x < 0)
		return 0;
	if(x > 3)
		return 3;
	return x;
}

int validateY(int y){
	if(y < 0)
		return 0;
	if(y > 1)
		return 1;
	return y;
}

double MDP(int x, int y){
	double Vn, Ve, Vw, Vs, maxNS, maxWE;
	Vn = Ve = Vw = Vs = 0;
	Vn = (matrix[validateY(y - 1)][validateX(x)] * 0.8) + (matrix[validateY(y + 1)][validateX(x)] * 0.2);
	Ve = (matrix[validateY(y)][validateX(x + 1)] * 0.8) + (matrix[validateY(y)][validateX(x - 1)] * 0.2);
	Vw = (matrix[validateY(y)][validateX(x - 1)] * 0.8) + (matrix[validateY(y)][validateX(x + 1)] * 0.2);
	Vs = (matrix[validateY(y + 1)][validateX(x)] * 0.8) + (matrix[validateY(y - 1)][validateX(x)] * 0.2);
	maxNS = Vn > Vs ? Vn : Vs;
	maxWE = Vw > Ve ? Vw : Ve;
	if(maxNS > maxWE){
		if(Vn > Vs){
			moveMatrix[y][x] = 'N';
		}
		else{
			moveMatrix[y][x] = 'S';
		}
	}
	else{
		if(Vw > Ve){
			moveMatrix[y][x] = 'W';
		}
		else{
			moveMatrix[y][x] = 'E';
		}
	}
	return maxNS > maxWE ? maxNS - 4 : maxWE - 4;
}

int main(){

	int x, y, z;

	for(z = 0 ; z < 10000 ; z++){
		for(y = 0; y < 2 ; y++){
			for(x = 0; x < 4 ; x++){
				if(!(x == 0 && y == 1) && !(x == 3 && y == 1)){
					newMatrix[y][x] = MDP(x, y);
				}
			}
		}
		if(memcmp(newMatrix, matrix, sizeof(matrix)) == 0){
			printf("Converge after %d iterations.\n\n", z + 1);
			break;
		}
		for(y = 0; y < 2 ; y++){
			for(x = 0; x < 4 ; x++){
				matrix[y][x] = newMatrix[y][x];
			}
		}
	}

	printf("Converged Matrix\n");
	for(y = 0; y < 2 ; y++){
		for(x = 0; x < 4 ; x++){
			printf("%lf\t", newMatrix[y][x]);
		}
		printf("\n");
	}

	printf("\n\nOptimal Policy\n");
	for(y = 0; y < 2 ; y++){
			for(x = 0; x < 4 ; x++){
				printf("%c\t", moveMatrix[y][x]);
			}
			printf("\n");
		}

	return 0;
}
