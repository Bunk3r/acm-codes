#include<stdio.h>
#include<list.h>

typedef struct nodo{
	char num;
	char fin;
	list<struct nodo> next;
}Nodo;

Nodo beg[10];
char MAL;

int main(){

	int Casos, Caso = 0, Numeros, Num = 0, inputPos;
	char input, missing;
	Nodo * act, tmp;

	freopen("input.in", "r", stdin);

	scanf("%d", &Casos);

	for( ; Caso < Casos ; Caso++){
		scanf("%d\n", &Numeros);
		for( ; Num < Numeros ; Num++){
			inputPos = 0;
			while((input = getchar()) >= '0' && input <= '9'){
				if(inputPos){
					missing = 1;
					for(list<Nodo>::iterator p = (* act).next.begin() ; p != (* act).next.end() ; p++){
						if((* p).num == input){
							act = &(* p);
							missing = 0;
						}
					}
					if(missing){
						tmp.num = input;
						(* act).next.push_back(tmp);
						act = &(* (* act).next.begin());
					}
				}
				else{
					if(beg[input - '0'].fin){
						MAL = 1;
					}
					else{
						act = &beg[input - '0'];
					}
				}
				inputPos++;
			}
			(* act).fin = 1;
		}
		printf("%s\n", MAL ? "NO" : "YES");
	}

	return 0;
}
