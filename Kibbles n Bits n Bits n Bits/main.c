#include<stdio.h>
#include<string.h>

void Hex2Bin(int num, char *bin){
    int pos;
    for(pos = 12 ; pos ; pos--){
    bin[pos] = num&1 ? '1' : '0';
    num >>= 1;
    }
    bin[13] = 0;
    bin[0] = '0';
}

int main(){

	char signo, bin1[14], bin2[14];
        int N, z, num1, num2;

        freopen("input.in", "r", stdin);

        scanf("%d", &N);
        for(z = 0 ; z < N ; z++){
               scanf("%X %c %X", &num1, &signo, &num2);
               Hex2Bin(num1, bin1);
               Hex2Bin(num2, bin2);
               if(signo == '+')
                    printf("%s + %s = %d\n", bin1, bin2, num1 + num2);
               else
                    printf("%s - %s = %d\n", bin1, bin2, num1 - num2);
               
	}

	return 0;
}
