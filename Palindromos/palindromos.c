#include<stdio.h>


int main(){

	char Cadena[1000];
	char Letra[] = "AEHIJLMOSTUVWXYZ12358";
	char Espejos[] = "A3HILJMO2TUVWXY51SEZ8";
	int Max;
	int w;
	int x;
	int y;
	int z;
	int P;
	int M;

	while(scanf("%s", &Cadena[0]) != EOF){
		Max = 0;
		P = 1;
		M = 1;
		while(Cadena[Max]){
			Max++;
		}
		Max--;

		for(x = 0, y = Max ; x < y ; x++, y--)
			if(Cadena[x] != Cadena[y])
				P = 0;

		for(x = 0, y = Max; x <= y ; x++, y--){
				w = 0;
				z = 0;
				while(Letra[w]){
					if(Cadena[x] == Letra[w]){
						z = 1;
						if(Cadena[y] != Espejos[w]){
							M = 0;
							break;
						}
					}
					w++;
				}
				if(!z)
					M = 0;
			}

		if(P && !M)
			printf("%s -- is a regular palindrome.\n", Cadena);
		else if(!P && M)
			printf("%s -- is a mirrored string.\n", Cadena);
		else if(!P && !M)
			printf("%s -- is not a palindrome.\n", Cadena);
		else if(P && M)
			printf("%s -- is a mirrored palindrome.\n", Cadena);
	}
	return 0;
}
