#include<stdio.h>
#include<string.h>

int main(){
	int n, x, y, caso = 1;
	char judge[50], equipo[50], flag, space;

	freopen("input.in", "r", stdin);

	scanf("%d%c", &n, &space);
	while(n--){
		space = x = 0;
		while(scanf("%c", &equipo[x]) && equipo[x] != '\n')
			if(equipo[x++] == ' ')
				space = 1;
		equipo[x] = '\0';
		gets(judge);
		x = strlen(judge);
		judge[x] = '\0';

		if(strcmp(judge, equipo) == 0 && !space)
			printf("Case %d: Yes\n",caso++);
		else{
			flag = y = x = 0;
			while(judge[x] && !flag){
				if(equipo[y] != ' '){
					if(equipo[y++] != judge[x++])
						flag = 1;
				}
				else
					y++;
			}

			while(equipo[y] != '\0' && !flag)
				if(equipo[y] == ' ')
					y++;
				else
					flag = 1;

			if(flag)
				printf("Case %d: Wrong Answer\n",caso++);
			else
				printf("Case %d: Output Format Error\n",caso++);
		}
	}

	return 0;
}
