#include<stdio.h>

int main(){
	int T, A, B, CASO = 1, total;
	freopen("input.in", "r", stdin);
	scanf("%d", &T);
	while(T--){
		scanf("%d %d", &A, &B);
		for(total = 0, A = A & 1 ? A : A + 1 ; A <= B ; A+=2)
			total += A;
		printf("Case %d: %d\n", CASO++, total);
	}
	return 0;
}
