#include<stdio.h>
#include<stdlib.h>

typedef struct{
	int numero;
	int modulo;
}nodo;

nodo numeros[10010];

int cmpNumeros(const void * A, const void * B){
	if((*(nodo *)A).modulo == (*(nodo *)B).modulo){
		if((*(nodo *)A).numero&1 && (*(nodo *)B).numero&1){
			return (*(nodo *)B).numero - (*(nodo *)A).numero;
		}
		else if((*(nodo *)A).numero&1 && !(*(nodo *)B).numero&1){
			return -1;
		}
		else if(!(*(nodo *)A).numero&1 && (*(nodo *)B).numero&1){
			return 1;
		}
		else{
			return (*(nodo *)A).numero - (*(nodo *)B).numero;
		}
	}
	return (*(nodo *)A).modulo - (*(nodo *)B).modulo;
}

int main(){

	int N, M, x;

	freopen("input.in", "r", stdin);

	while(scanf("%d %d", &N, &M)){
		printf("%d %d\n", N, M);

		if(N == 0 && M == 0)
			break;

		for(x = 0 ; x < N ; x++){

			scanf("%d", &numeros[x].numero);
			numeros[x].modulo = numeros[x].numero % M;

		}

		qsort(numeros, N, sizeof(nodo), cmpNumeros);

		for(x = 0 ; x < N ; x++){
			printf("%d %d\n", numeros[x].numero, numeros[x].modulo);
		}

	}

	return 0;
}
