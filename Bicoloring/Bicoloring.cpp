#include<stdio.h>

struct Graph{
	int Next[200];
	char C;
	int ad;
};

char Bicoloring;
char Visited[200];
struct Graph Bicolor[200];

int main(){
	void pintar(int x, char Color);
	char Repetido;
	int n;
	int l;
	int N1;
	int N2;
	int x;
	int y;


	while(scanf("%d", &n) && n != 0){

		Bicoloring = '1';
		scanf("%d", &l);

		for(x = 0 ; x < n ; x++){
			Bicolor[x].C = 'B';
			Bicolor[x].ad = 0;
		}

		for(x = 0; x < l ; x++){
			scanf("%d %d", &N1, &N2);
			Repetido = '1';
			y = 0;
			while(y < Bicolor[N1].ad){
				if(Bicolor[N1].Next[y] == N2){
					Repetido = '\0';
					break;
				}
				y++;
			}
			if(Repetido){
				Bicolor[N1].Next[Bicolor[N1].ad] = N2;
				Bicolor[N1].ad++;
				Bicolor[N2].Next[Bicolor[N2].ad] = N1;
				Bicolor[N2].ad++;
			}
		}

		for(x = 0 ; x < n ; x++)
			Visited[x] = '\0';

		for(x = 0 ; x < n ; x++)
			if(Bicoloring && !Visited[x])
				pintar(x,'R');

		if(Bicoloring)
			printf("BICOLORABLE.\n");
		else
			printf("NOT BICOLORABLE.\n");

	}

	return 0;
}

void pintar(int x, char Color){
		int y;
		Bicolor[x].C = Color;
		Visited[x] = '1';
		for(y = 0 ; y < Bicolor[x].ad ; y++)
			if(Bicoloring)
				if(!Visited[Bicolor[x].Next[y]])
					pintar(Bicolor[x].Next[y], Color == 'R' ? 'A':'R');
				else if(Bicolor[Bicolor[x].Next[y]].C == Color)
						Bicoloring = '\0';
}
