#include<stdio.h>
#include<string.h>
#include <ctype.h>

/*
 *
 * QUESTION --- the first conference on the topic of artificial intelligence was held at dartmouth college in this year
 *
 * ANSWER = 1956
 *
 */


char text[27][114] = {};

int main(){

	int pos = 1, act;

	freopen("Ex1.input", "r", stdin);

	gets(text[0]);
	printf("%s\n", text[0]);

	for(act = 0 ; text[0][act] ; act++){
		text[0][act] = tolower(text[0][act]);
	}

	printf("0.- %s\n", text[0]);


	for( ; pos < 27 ; pos++){
		memcpy(text[pos], text[pos - 1], sizeof(text[pos - 1]));
		for(act = 0 ; text[pos][act] ; act++){
			if(text[pos][act] != ' '){
				text[pos][act] = 'a' + (text[pos][act] + pos - 'a') % 26;
			}
		}
		printf("%d.- %s\n", pos, text[pos]);
	}



	return 0;
}
