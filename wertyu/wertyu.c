#include<stdio.h>

int main(){

	char original[] = " 1234567890-=WERTYUIOP[]\\SDFGHJKL;'XCVBNM,./\n";
	char nuevo[] =    " `1234567890-QWERTYUIOP[]ASDFGHJKL;ZXCVBNM,.\n";
	char letra;
	int x;


	while(scanf("%c", &letra) != EOF)
		for(x = 0 ; original[x] ; x++)
			if(original[x] == letra){
				printf("%c", nuevo[x]);
				break;
			}

	return 0;
}
