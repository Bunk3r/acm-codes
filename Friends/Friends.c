#include<stdio.h>
#include<stdlib.h>

struct Lista{
	struct Lista *Siguiente;
	int id;
	struct Lista *Ultimo;
};

char Visitados[30001];
typedef struct Lista list;
list Grupo[30000];
list Actual;

int main(){
	int casos;
	int N;
	int M;
	int x;
	int A1;
	int A2;
	list *aux, *p;
	void contar(int x, int amistades);

	freopen("input.in", "r", stdin);

	scanf("%d", &casos);

	while(casos-- != 0){

		scanf("%d %d", &N, &M);

		for(x = 0 ; x <= N ; x++)
			Visitados[x] = '\0';

		for(x = 0 ; x < M ; x++){
			scanf("%d %d", &A1, &A2);
			aux = (list *)malloc(sizeof(list));
			aux->id = A1;
			aux->Siguiente = NULL;
		}

		for(x = 1 ; x <= N ; x++)
			if(!Visitados[x])
				contar(x, 1);

	}
	return 0;
}

void contar(int x, int amistades){
	list Actual;
	Visitados[x] = '1';
	Actual = *Grupo[x].Siguiente;
	while(Actual.id){
		Actual = *Grupo[x].Siguiente;
		contar(Actual.id , ++amistades);
		Actual = *Actual.Siguiente;
	}
}
