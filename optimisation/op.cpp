#include<cstdio>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

vector<int> arbol[100];
vector<int>::iterator p;
int anterior[100];
int grado[100];

int dependencia(int target, int from){
	int x = target;
	while(x != 1){
		int y = from;
		while(y != 1){
			if(y == x)
				return x;
			else
				y = anterior[y];
		}
		x = anterior[x];
	}
	return 1;
}

void recorrer(int actual){
	vector<int>::iterator q;
	sort(arbol[actual].begin(), arbol[actual].end());
	q = arbol[actual].begin();
	while(q != arbol[actual].end())
		printf("%d %d\n", actual,*q++);
	q = arbol[actual].begin();
	while(q != arbol[actual].end())
		recorrer(*q++);
}

int main(){
	int n, x, y, val;
	freopen("input.in", "r", stdin);
	while(scanf("%d", &n) != EOF){
		memset(grado, 0, sizeof(grado));
		memset(anterior, 0, sizeof(anterior));
		grado[1] = 1;
		for(x = 1; x <= n ; x++)
			for(y = 1 ; y <= n ; y++){
				scanf("%d", &val);
				if(val){
					if(anterior[y]){
						if(grado[x] < grado[y] && grado[x]){
							p = arbol[anterior[y]].begin();
							while(*p != y)
								p++;
							arbol[anterior[y]].erase(p);
							anterior[y] = dependencia(y, x);
							arbol[anterior[y]].insert(arbol[anterior[y]].end(),y);
						}
					}
					else{
						if(y != 1){
							arbol[x].push_back(y);
							anterior[y] = x;
							grado[y] = grado[x] + 1;
						}
					}
				}
			}
		recorrer(1);
		for(x = 1 ; x <= n ; x++)
			arbol[x].erase(arbol[x].begin(), arbol[x].end());
	}
	return 0;
}
