#include<stdio.h>
#include<string.h>
#include<windows.h>

char entrada[30];
int flag, move, Ta = 0,Tb = 0,Td = 0,Tc = 0, Md, posD;

void verifica(int pos, int move);

void imprime(int posE){
	Sleep(1000);
	posE++;
	int x;
	/***********************************/
	for(x = 0; x < 8 ; x++) printf("\n");
	for(x = 0; x < 20 ; x++) printf("____");
	/***********************************/
	for(x = 0,printf("\n "); entrada[x] != '<' ;x++)printf("| %c ", entrada[x]);
	for(x = 0,printf("| < |\n"); x < 20 ; x++) printf("----");
	for(x = 1; x < posE * 4 ; x++)printf(" ");
	for(x = 1,printf("^\n"); x < posE * 4 ; x++)printf(" ");
	printf("|\n");
	/***********************************/
	for(x = 0; x < 9 ; x++) printf("\n");
}

void clear(){
	int x;
	for(x = 0; x < 25;x++)
		printf("\n");
}

void verificaD(int pos, int paredes, int move){
	if(Tc == 1)
		flag = 0;
	else if(paredes < 2)
		switch(entrada[pos]){
		case '<':
			paredes++;
			imprime(pos);
			move = -1;
			verificaD(pos + move, paredes, move);
			break;
		case '>':
			paredes++;
			imprime(pos);
			move = 1;
			verificaD(pos + move, paredes, move);
			break;
		case 'd':
			entrada[pos] = '#';
			imprime(pos);
			posD = pos + move;
			Md = move;
			break;
		case '#':
		case 'a':
		case 'b':
		case 'c':
			imprime(pos);
			verificaD(pos + move, paredes, move);
			break;
		default:
			flag = 0;
		}
	else{
		posD = pos + move;
		Md = move;
		Td = 1;
	}
}

void verificaC(int pos, int paredes, int move){
	if(paredes < 2){
		switch(entrada[pos]){
		case '<':
			paredes++;
			imprime(pos);
			move = -1;
			verificaC(pos + move, paredes,move);
			break;
		case '>':
			paredes++;
			imprime(pos);
			move = 1;
			verificaC(pos + move, paredes, move);
			break;
		case 'c':
			entrada[pos] = '#';
			imprime(pos);
			verifica(pos + move,move);
			break;
		case '#':
		case 'a':
		case 'b':
		case 'd':
			imprime(pos);
			verificaC(pos + move, paredes, move);
			break;
		default:
			flag = 0;
		}
	}
	else if(Td != 1 || Ta != 1 || Tb != 1)
			flag = 0;
}

void verificaB(int pos, int paredes, int move){
	if(Tc == 1)
		flag = 0;
	else if(paredes < 2){
			switch(entrada[pos]){
			case '<':
				paredes++;
				imprime(pos);
				move = -1;
				verificaB(pos + move, paredes, move);
				break;
			case '>':
				paredes++;
				imprime(pos);
				move = 1;
				verificaB(pos + move, paredes, move);
				break;
			case 'b':
				entrada[pos] = '#';
				imprime(pos);
				if(Tc != 1)
					verificaC(pos + move, 0, move);
				else
					flag = 0;
				break;
			case '#':
			case 'a':
			case 'c':
			case 'd':
				imprime(pos);
				verificaB(pos + move, paredes, move);
				break;
			default:
				flag = 0;
			}
		}
		else{
			Tb = 1;
			if(Tc != 1){
				imprime(pos);
				verificaC(pos + move, 0, move);
			}
			else
				flag = 0;
		}
}

void verificaA(int pos, int paredes, int move){
	if(Tb == 1 || Tc == 1)
		flag = 0;
	else if(paredes < 2){
			switch(entrada[pos]){
			case '<':
				paredes++;
				imprime(pos);
				move = -1;
				verificaA(pos + move, paredes, move);
				break;
			case '>':
				paredes++;
				imprime(pos);
				move = 1;
				verificaA(pos + move, paredes, move);
				break;
			case 'a':
				entrada[pos] = '#';
				imprime(pos);
				verificaB(pos + move, 0, move);
				break;
			case '#':
			case 'b':
			case 'c':
			case 'd':
				imprime(pos);
				verificaA(pos + move, paredes, move);
				break;
			default:
				flag = 0;
			}
		}
		else{
			Ta = 1;
			if(Tb != 1){
				imprime(pos);
				verificaB(pos + move, 0, move);
			}
			else
				flag = 0;
		}
}


void verifica(int pos, int move){
	if(Td != 1){
		verificaD(pos,0,move);
		pos = posD;
		move = Md;
	}
	if(Ta == 1){
		if(Tb == 1)
				verificaC(pos,0,move);
		else
				verificaB(pos,0,move);
	}
	else
		verificaA(pos, 0, move);
}

int main(){
	int x = 1, a = 0, b = 0, c = 0,d = 0;
	flag = 1;
	memset(entrada, '<', sizeof(entrada));
	entrada[0] = '>';
	printf("Ingrese la cadena a validar(max. 18 caracteres):\n");
	while(scanf("%c", &entrada[x]) && entrada[x] != '\n' && x < 19)
		switch(entrada[x++]){
		case 'a':
			a++;
			break;
		case 'b':
			b++;
			break;
		case 'c':
			c++;
			break;
		case 'd':
			d++;
			break;
		default:
			flag = 0;
		}
	entrada[x] = '<';
	entrada[20] = '\0';
	clear();
	verificaA(0,0,1);
	Sleep(1000);
	clear();

	if(flag)
		if(a < b && b < c && d < c)
			printf("\n\n\n\n\n\t\tAceptado");
		else
			printf("\n\n\n\n\n\t\tNo aceptado");
	else
		printf("No aceptado");
	Sleep(2000);
	return 0;
}
