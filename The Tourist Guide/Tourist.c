#include<stdio.h>
#include<math.h>

struct city{
	int Next[101][2];
	int ad;
};

struct city Map[101];
int maximo = 0;
int Known[101];
int D;


int main(){
	int N;
	int R;
	int S;
	int T;
	int x;
	int y;
	int caso = 1;
	int C1;
	int C2;
	int Limit;
	char Repetido;
	int minimo = 0;
	void Buscar(int Actual, int Limite);

	while(scanf("%d %d", &N, &R) && !(N == 0 && R == 0)){

		if(caso != 1){
			minimo = 0;
			maximo = 0;
		}

		for(x = 1 ; x <= N ; x++){
			Map[x].ad = 0;
			Known[x] = 0;
		}

		for(x = 0 ; x < R ; x++){
			scanf("%d %d %u", &C1, &C2, &Limit);
			y = 0;
			Repetido = '\0';
			while(y < Map[C1].ad){
				if(Map[C1].Next[y][0] == C2){
					Repetido = 'Y';
					if(Map[C1].Next[y][1] < Limit)
						Map[C1].Next[y][1] = Limit;
					else
						y = 200;
				}
				else
					y++;
			}
			if(!Repetido){
				Map[C1].Next[Map[C1].ad][0] = C2;
				Map[C1].Next[Map[C1].ad][1] = Limit;
				Map[C1].ad++;
				Map[C2].Next[Map[C2].ad][0] = C1;
				Map[C2].Next[Map[C2].ad][1] = Limit;
				Map[C2].ad++;
			}
			else{
				y = 0;
				while(y < Map[C2].ad){
					if(Map[C2].Next[y][0] == C1)
						Map[C2].Next[y][1] = Limit;
					y++;
				}
			}
		}

		scanf("%d %d %d", &S, &D, &T);


		if(S != D){
			Buscar(S, 9999999);
			minimo = ceil((float)T / (maximo - 1));
		}
		else
			minimo = 1;

		printf("Scenario #%d\nMinimum Number of Trips = %d\n\n", caso++, minimo);

	}

	return 0;
}


void Buscar(int Actual, int Limite){
	int x = 0;
	if(Known[Actual] < Limite){
		Known[Actual] = Limite;
		if(Actual != D)
			while(x < Map[Actual].ad){
				Buscar(Map[Actual].Next[x][0], Limite > Map[Actual].Next[x][1] ? Map[Actual].Next[x][1] : Limite );
				x++;
			}
		else
			maximo = maximo > Limite ? maximo : Limite;
	}
}
