#include<stdio.h>
#include<string.h>

char matrix[10][10], tmp[10][10];

int stuck, kill, win, iter;

void simularM(), simularA();

void simularA(){
	if(iter < 33){
		int x, y;
		for(x = 1 ; x < 9 ; x++)
			for(y = 1 ; y < 9 ; y++)
				if(tmp[x][y] == 'A'){
					if((matrix[x + 1][y] == '.' || matrix[x + 1][y] == 'M' || matrix[x + 1][y] == 'P') && tmp[x + 1][y] != 'A')matrix[x + 1][y] = 'A';
					if((matrix[x - 1][y] == '.' || matrix[x - 1][y] == 'M' || matrix[x - 1][y] == 'P') && tmp[x - 1][y] != 'A')matrix[x - 1][y] = 'A';
					if((matrix[x][y + 1] == '.' || matrix[x][y + 1] == 'M' || matrix[x][y + 1] == 'P') && tmp[x][y + 1] != 'A')matrix[x][y + 1] = 'A';
					if((matrix[x][y - 1] == '.' || matrix[x][y - 1] == 'M' || matrix[x][y - 1] == 'P') && tmp[x][y - 1] != 'A')matrix[x][y - 1] = 'A';
				}
		if(!kill && !stuck && !win){
			memcpy(tmp, matrix, 100);
			simularM();
		}
	}
	else
		stuck = 1;
}

void simularM(){
	int x, y, Ms = 0;
	iter++;
	if(iter < 33){
		for(x = 1 ; x < 9 ; x++)
			for(y = 1 ; y < 9 ; y++)
				if(tmp[x][y] == 'M'){
					Ms++;
					if((matrix[x + 1][y] == '.' || matrix[x + 1][y] == 'M') && tmp[x + 1][y] != 'M')matrix[x + 1][y] = 'M';
					else if(matrix[x + 1][y] == 'P')win = 1;
					if((matrix[x - 1][y] == '.' || matrix[x - 1][y] == 'M') && tmp[x - 1][y] != 'M')matrix[x - 1][y] = 'M';
					else if(matrix[x - 1][y] == 'P')win = 1;
					if((matrix[x][y + 1] == '.' || matrix[x][y + 1] == 'M') && tmp[x][y + 1] != 'M')matrix[x][y + 1] = 'M';
					else if(matrix[x][y + 1] == 'P')win = 1;
					if((matrix[x][y - 1] == '.' || matrix[x][y - 1] == 'M') && tmp[x][y - 1] != 'M')matrix[x][y - 1] = 'M';
					else if(matrix[x][y - 1] == 'P')win = 1;
				}
		if(!kill && !stuck && !win){
			if(Ms)
				simularA();
			else
				kill = 1;
		}
	}
	else
		stuck = 1;
}

int main(){
	int N, x, y;
	freopen("input.in", "r", stdin);
	scanf("%d\n", &N);
	memset(matrix, 0, 100);
	while(N--){
		for(x = 1; x < 9 ; x++,scanf("\n"))
			for(y = 1 ; y < 9 ; y++)
				scanf("%c", &matrix[x][y]);
		stuck = kill = iter = win = 0;
		memcpy(tmp,matrix, 100);
		simularM();
		if(win)
			printf("You can escape.\n");
		else if(kill)
			printf("You are eliminated.\n");
		else
			printf("You are trapped in the Matrix.\n");
	}
	return 0;
}
